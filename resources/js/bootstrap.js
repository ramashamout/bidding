window._ = require('lodash');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo';

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    forceTLS: false,
    wsHost: window.location.hostname,
    wsPort: 6001
});
//
// window.Echo.private('result.2')
//     .listen('PodcastResult',(e) => {
//         console.log(e.result);
//        //  document.getElementsByClassName('bidding')[0].innerHTML='<h1>'+e.bid.bid_amount+'</h1>';
//        //  document.getElementsByClassName('bb')[0].innerHTML=' <p>'+e.bid.bidder +' Bidding</p>';
//        //  document.getElementsByClassName('nn')[0].innerHTML=' <p>at :'+e.bid.bid_datetime+'</p>';
//
//     })
window.Echo.channel('home')
    .listen('NewMessage',(e) => {
       // console.log(e.bid);
        document.getElementsByClassName('bidding')[0].innerHTML=e.bid.bid_amount+' SYR ';
        document.getElementsByClassName('bidder')[0].innerHTML=' قام '+e.bid.bidder+' بالمزايدة في تاريخ  '+e.bid_date+' تمام الساعة  '+e.bid_time+'';


    })
window.Echo.channel('time')
    .listen('PodcastTime',(e) => {
        console.log(e.hour);
        //document.getElementsByClassName('tim2')[0].innerHTML='<p id="demo2">extra </p>';
        var countDownDate = new Date(e.month+' '+e.day+', '+e.year+' '+e.hour+': '+e.minute+': '+e.second).getTime();
       // var countDownDate = new Date("{{$month}} {{$day}}, {{$year}} {{$hour+3}}: {{$minute}}: {{$second}}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now ;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById("demo2").innerHTML ="وقت إضافي : "+ days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo2").innerHTML = "EXPIRED";
            }
        }, 1000);

    })
// window.Echo.join(`home`)
//     .here((users) => {
//        // $('#online-users').append('<p>kkkkkkkkkkkkkkkkkkkkkkkkkkk</p>')
//         console.log(users);
//         for(var i=0 ; i<users.length ; i++){
//             document.getElementsByClassName('list-group')[0].innerHTML+=' <li class="list-group-item" id='+users[i].id+'>'+users[i].name+'</li>';
//
//         }
//        })
//     .joining((user) => {
//         console.log(user.name);
//       //  document.getElementsByClassName('list-group')[0].innerHTML+=' <li class="list-group-item" id='+user.id+'>'+user.name+'</li>';
//          if(!document.getElementsByClassName('list-group')[0].innerHTML.includes(' <li class="list-group-item" id='+user.id+'>'+user.name+'</li>'))
//          {
//          }else {
//              document.getElementsByClassName('list-group')[0].innerHTML+=' <li class="list-group-item" id='+user.id+'>'+user.name+'</li>'
//          }
//
//     })
//     .leaving((user) => {
//         console.log(user.name);
//         document.getElementsByClassName('list-group')[0].innerHTML = document.getElementsByClassName('list-group')[0].innerHTML.replace(' <li class="list-group-item" id='+user.id+'>'+user.name+'</li>', ' ');
//     });

