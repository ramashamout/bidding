require('./bootstrap');

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();

// Echo.channel('home')
//     .listen('NewMessage',(e) => {
//         console.log(e.message);
//     })
// Echo.join(`home`)
//     .here((users) => {
//         $('#online-users').append(' <li class="list-group-item">Cras justo odio</li>')
//     })
//     .joining((user) => {
//         console.log(user.name);
//     })
//     .leaving((user) => {
//         console.log(user.name);
//     })
//     .error((error) => {
//         console.error(error);
//     });
