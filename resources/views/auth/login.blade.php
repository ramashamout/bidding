@extends('master1')
@section('contents')

      <!-- ======= Why Us Section ======= -->
       <section id="why-us" class="why-us section-bg"style="margin-top:30px">
         <div class="container-fluid" data-aos="fade-up">
            <div class="row ">
               <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                  <section id="contact" class="contact">
                     <div  class="container"  >
                        <!--  role="form" class="php-email-form" -->
                        <form  method="POST" action="{{ route('login') }}" class="display">
                            @csrf
                           <div class="row d-flex justify-content-center align-items-center">
                              <header class="section-header">
                                 <h3>تسجيل الدخول</h3>
                              </header>
                              <div class="form-group col-md-8" style="margin-top: 20px;">
                                @if (session('error'))
                              <div class="alert alert-danger">
                                {{ session('error') }}
                              </div>
                           @endif
                                 <div class="form-outline mb-4">
                                    <input name="email" :value="old('email')"  class="form-control" type="text" id ="email"placeholder="البريد الإلكتروني" required autofocus>
                                 </div>
                                 <div class="form-outline mb-4">
                                    <input class="form-control" type="password" name="password" autocomplete="current-password" 
                                    id ="password" placeholder="كلمة المرور" required >
                                 </div>
                              </div>
                           </div>
                          
                           <div class="text-center"><button type="submit" id="customer_login" name="customer_login">تسجيل الدخول</button></div>

                           <h6 class="title text-center" style="padding:10px;"><a href="/register">إنشاء حساب</a></h6>
                        </form>
                     </div>
                  </section>
               </div>
               <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/login2.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
            </div>
         </div>
      </section>
 @endsection