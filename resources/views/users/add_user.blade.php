@extends('master1')
@section('contents')
      <!-- ======= Why Us Section ======= -->
      <section id="why-us" class="why-us section-bg"style="margin-top:30px">
         <div class="container-fluid" data-aos="fade-up">
            <div class="row ">
               <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                  <section id="contact" class="contact">
                     <div  class="container"  >
               <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data" >
                         @csrf
                           <div class="row d-flex justify-content-center align-items-center">
                              <header class="section-header">
                                 <h3> إنشاء حساب</h3>
                              </header>
                              <div class="form-group col-md-8" style="margin-top: 15px;">
                                 <div class="form-outline mb-4" >
                                    {!! Form::text('name', null, array('placeholder' => 'الاسم','class' => 'form-control mx-sm-3')) !!}
                                 </div>
                                 <div class="form-outline mb-4">
                                     {!! Form::text('email', null, array('placeholder' => 'الإيميل ','class' => 'form-control mx-sm-3')) !!}
                                 </div>
                                 <div class="form-outline mb-4">
                                    {!! Form::password('password', array('placeholder' => 'كلمة المرور ','class' => 'form-control mx-sm-3')) !!}
                                 </div>
                                  <div class="form-outline mb-4">
                                    {!! Form::password('confirm-password', array('placeholder' => 'تأكيد كلمة المرور ','class' => 'form-control mx-sm-3')) !!}
                                 </div>
                                      <div class="form-group" style="width: 220px;" >
           
      </div>
                              </div>
                           </div>
                           <div class="text-center"><button type="submit">إنشاء </button></div>

                           <h6 class="title text-center" style="padding:10px;"><a href="/login">تسجيل الدخول </a></h6>
                        </form>
                     </div>
                  </section>
               </div>
               <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/register2.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
            </div>
         </div>
      </section>
   @endsection