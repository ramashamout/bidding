@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="/assets/img/dashboard.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>تعديل الصلاحية </h2>

    </div>
    
    <div class="row tab-content" data-aos="fade-up" data-aos-delay="200">
    
     
   
     <!-- all action -->
   <div class="row">

          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch ">
                {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id],'class'=>'display']) !!}
                <div class="row">
                 
                <div class="form-group">
       
              
                <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="name" value="{{$role->name}}" />
                                               
                </div>
               
                  @foreach($permission as $value)
                  <div class="form-group col-lg-3 col-md-3" style="float: right;margin-top:30px;">
                   
                   {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }} {{ $value->name }}
                  </div>
                    @endforeach


              </div>


              <div class="text-center" style="margin-top:60px;"><button type="submit">حفظ</button></div>

                       
           {!! Form::close() !!}
          </div>
        </div>
       
       </div>
  </div>
</section>
@endsection
