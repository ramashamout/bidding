@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="/assets/img/dashboard.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>إدارة  الصلاحيات</h2>
    </div>
    <ul class="nav nav-pills mb-3 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100"role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">إضافة صلاحية </button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">عرض الصلاحيات</button>
      </li>
      
    </ul>
    <div class="row tab-content" data-aos="fade-up" data-aos-delay="200">
      <!-- add action -->
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="row">
          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch">
            {!! Form::open(array('route' => 'roles.store','method'=>'POST','class'=>'display')) !!}
            <div class="row">
              
              <div class="form-group">
                {!! Form::text('name', null, array('placeholder' => 'الاسم ','class' => 'form-control')) !!}
              </div>
              
              @foreach($permission as $value)
              <div class="form-group col-lg-3 col-md-3" style="float: right;margin-top:30px;">
                
                {{Form::checkbox('permission[]',$value->id,false, array('class' => 'name'))}}{{$value->name}}
              </div>
              @endforeach
            </div>
            <div class="text-center" style="margin-top:60px;"><button type="submit">حفظ</button></div>
            
            {!! Form::close() !!}
          </div>
        </div>
      </div>
      
      
      <!-- all action -->
      <div class="tab-pane fade " id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        <div class="row">
          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch display">
            <table class="table table-striped table-advance table-hover ">
              
              <hr>
              <thead>
                <tr>
                  <th class="text-center"><i class="bi bi-person-fill" ></i>الصلاحية</th>
                  
                  <th class="text-center">الأذونات</th>
                  
                  <th></th>
                  
                </tr>
              </thead>
              <tbody>
                
                @foreach ($roles as $role)
                
                
                <?php
                $rolePermissions = Spatie\Permission\Models\Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
                ->where("role_has_permissions.role_id",$role->id)
                ->get();
                ?>
                
                <tr>
                  <td class="text-center">
                    
                    <h6 style=" color: #37517e;">{{$role->name}}</h6>
                    
                  </td>
                  
                  
                  
                  
                  <td class="text-center">
                    <p>
                      <a class="btn btn-primary btn-xs " data-toggle="collapse" href="#collapseExample{{$role->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class=" bi bi-eye"></i>
                      </a>
                    </p>
                    
                    <div class="collapse" id="collapseExample{{$role->id}}">
                      @foreach ($rolePermissions as $rolePermission)
                      <div class="form-group col-lg-12 col-md-12" >
                        <ul class="task-list" >
                          
                          <a style="color: black;">{{$rolePermission['name']}}</a>
                          
                          
                        </ul>
                      </div>
                      @endforeach
                    </div>
                    
                  </td>
                  
                  <td class="text-center">
                    <a  class="btn btn-primary btn-xs"  href="{{ route('roles.edit', $role->id) }}">
                      <i class=" bi bi-pencil  btn-xs"></i>
                    </a>
                    
                    
                  </td>
                  
                </tr>
                
                
                @endforeach
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection