<!DOCTYPE html>
<html lang="ar-sa" dir="rtl">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title >MAZAD</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <!--select accessory css -->
      <link rel="stylesheet" type="text/css" href="/css/bootstrap-select.css">


    <!-- Favicons -->
    <link href="/assets/img/logo.png" rel="icon">
    <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Fonts -->
    <link rel="stylesheet" type="/text/css" href="...../fonts/DroidKufi/EARLY_ACCESS.css">
    <link rel="stylesheet" type="/text/css" href="fonts/AlexBrush/AlexBrush-Regular.ttf">
    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>

    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="/assets/css/style.css" rel="stylesheet">
     <!--select accessory css -->
       <link rel="stylesheet" type="text/css" href="/css/bootstrap-select.css">


  </head>
  <body>
   
    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top ">
      <div class="container d-flex ">

        <nav id="navbar" class="navbar ms-auto ">

          <ul>

            <li><a class="nav-link scrollto " href="/index">الصفحة الرئيسية </a></li>
            <li><a class="nav-link scrollto" href="/actions">المزادات</a></li>
            <li><a class="nav-link scrollto" href="{{route('main_s.index')}}">الأقسام</a></li>

            @auth

              @hasanyrole($roles)
           <li class="dropdown"><a href="#"><span>لوحة التحكم</span> <i class="bi bi-chevron-down"></i></a>
            <ul>


           @can('إدارة المستخدمين ')
            <li class="dropdown"><a ><span><i class="bi bi-chevron-right"></i>إدارة المستخدمين </span> </a>

                <ul>
             

                  <li><a href="/control_users">المستخدمين </a></li>
                  <li><a href="/control_users_role">المشرفين </a></li>
                  <li><a href="/roles/create">إدارة الصلاحيات</a> </li>
                </ul>
            </li>
            @endcan
            @can('إدارة الأقسام ')
               <li><a href="/all_section">إدارة  الأقسام</a></li>
            @endcan
            @can('إدارة التصنيفات ')
               <li><a href="/all_category">إدارة  التصنيفات</a></li>
            @endcan
            @can('إدارة المزادات  ')

              <li class="dropdown"><a ><span><i class="bi bi-chevron-right"></i>إدراة المزادات</span> </a>

                <ul>
                    @foreach($all_sections as $all_sections)

                  <li><a href="{{route('control_action',$all_sections->id)}}">{{$all_sections->name}}</a></li>
                  @endforeach

                </ul>

              </li>
             @endcan
            </ul>
          </li>
           @endhasanyrole
            <li><a class="getstarted scrollto" > {{auth()->user()->name}}</a></li>
          <!--start buttom login -->



                 <li>
             <form method="POST" action="{{ route('logout') }}" style="font-size:16px;">
                  @csrf
            <a class="getstarted scrollto" href="{{ route('logout') }}" onclick="event.preventDefault();
                        this.closest('form').submit(); " role="button">تسجيل الخروج </a>

            </form></li>

          @else
           <li><a class="getstarted scrollto  " href="/login" style="font-size:16px;">تسجيل الدخول</a></li>
           <li><a class="getstarted scrollto  " href="/register" style="font-size:16px;">إنشاء حساب</a></li>
           @endauth



          <!--start buttom login -->
          <!--start buttom logout -->
          <!-- <li><a class="getstarted scrollto " href="/about" style="font-size:18px;">RamaShammout &nbsp;<i class="bi bi-box-arrow-right" style="font-size:20px;"></i></a></li> -->
          <!--end buttom logout -->

        </ul>



        <i class="bi bi-list mobile-nav-toggle"></i>

        </nav>

        <!--  notification start -->
        <div class="notify-row" style="margin: 3px;">

          <div class="top-menu">
              @if(auth()->user()!=null && auth()->user()->type !='admin' && auth()->user()->type !='employ')
            <a id="toastbtn" >

              <i class="bi bi-bell" style="font-size: 25px;color: white"></i>

              <span class="badge bg-danger num" style="position: absolute; ">{{auth()->user()->unreadNotifications->count()}}</span>

            </a>
              @endif
              <script src="{{ asset('js/app.js') }}"></script>
          </div>
        </div>
        <div class="toast-container position-absolute top-0 end-0 p-3"  >
            @if(auth()->user()!=null && auth()->user()->type !='admin' && auth()->user()->type !='employ')
          @foreach(auth()->user()->unreadNotifications as $brideReminder)
          <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header" style="text-align: rtl;">
              <!-- <img src="..." class="rounded me-2" alt="..."> -->
              <div style="color: red;"> إشعار جديد</div>
              <!-- <small class="text-muted"></small> -->
              <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close" style="margin-right: 220px;"></button>
            </div>
            <div class="toast-body" style="text-align: rtl;" >

               
              <a href="{{ route('markAsRead',$brideReminder->id)}}" 
             > {{$brideReminder->data['content']}}</a>


            </div>
          </div>
          @endforeach
            @endif
                <script>
              window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{
                  console.log(e.result.content)
                  document.getElementsByClassName('num')[0].innerHTML= e.result.content;
                  
              })
          </script>
        </div>
        <!--  notification end -->
      </header>
      <!-- End Header -->

      @yield('contents')

       <script type="text/javascript" src="/js/jquery.min.js"></script>
     <script type="text/javascript" src="/js/bootstrap.bundle.min.js"></script>
     <script type="text/javascript" src="/js/bootstrap-select.min.js"></script>

      <!-- Vendor JS Files -->
      <script src="/assets/vendor/aos/aos.js"></script>
      <script src="/assets/vendor/jquery.min.js"></script>
      <script src="/assets/vendor/jquery-3.1.1.js"></script>
      <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
      <script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
      <script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
      <script src="/assets/vendor/waypoints/noframework.waypoints.js"></script>
      <script src="/assets/vendor/php-email-form/validate.js"></script>
      <!-- Template Main JS File -->
      <script src="/assets/js/main.js"></script>
      <script>
      document.getElementById("toastbtn").onclick = function() {
      var toastElList = [].slice.call(document.querySelectorAll('.toast'))
      var toastList = toastElList.map(function(toastEl) {
      // Creates an array of toasts (it only initializes them)
      return new bootstrap.Toast(toastEl) // No need for options; use the default options
      });
      toastList.forEach(toast => toast.show()); // This show them

      console.log(toastList); // Testing to see if it works
      };

      </script>
        <!-- select accessory js -->

    </body>
  </html>
