
@extends('bid.comm')

@section('main')
<body id="page5">
<!--==============================header=================================-->
<header>
    <div id="topnav" class="navbar navbar-fixed-top default">
        <div class="navbar-inner">
            <div class="container">
                <div class="logo">
{{--                    <a href="index.html"><img src="assets/img/logo.png" alt="" /></a>--}}
                    @if(Auth::user()!=null)
{{--                        <h1>current user</h1>--}}
                        <div>{{Auth::user()->name}}</div><br>
                    @endif
                </div>
                <div class="navigation">
                    <nav>
                        <ul class="nav pull-right">
                            <li ><a href="/dashboard">الصفحة الرئيسية</a></li>
                            <li class="current"><a href="{{route('bid.index')}}">المزاد الحالي</a></li>
                            <li ><a class="noti" href="{{ route('notif')}}"><p>الإشعارات{{auth()->user()->unreadNotifications->count()}}</p></a></li>
                            @if(auth()->id()==1)
                                <li><a href="{{ route('bid.create')}}">إضافة مزاد</a></li>
                            @endif
                            <script srs="{{asset('js/app.js')}}"></script>
                            <script src="js/app.js"></script>
                            <script>
                                window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{
                                    console.log(e.result.content)
                                    document.getElementsByClassName('noti')[0].innerHTML='<p>'+'الإشعارات'+e.result.content+'</p>';
                                })
                            </script>
                            <li>
                                <form method="POST" action="{{ route('logout') }}" x-data>
                                    @csrf
                                    <button href="{{ route('logout') }}"
                                            @click.prevent="$root.submit();">
                                        تسجيل الخروج
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- end top -->
</header>
<!--==============================content================================-->
<section id="content">
    <div class="main">
        <div class="indent-left">
            <div class="wrapper">
                <article class="col-1">
                    <br><br><br><br><br><br><br><br><br><br><br><br>
                    <div>
                        @if ($errors!=null)
                            <div class="alert alert-danger">
                                <ul>

                                    <li>
                                        {{ $errors }}
                                    </li>
                                </ul>
                            </div><br />
                        @endif

                    </div>

                    <div class="p1">
                        <figure class="img-border">
                            @foreach($bid->piece->images as $img)
                                 <img src='{{$img->piece_img}}' height="400" width="400"  alt="Our Work Image" class="img-fluid">
                            @endforeach
                        </figure>
                        <div class="clear"></div>
                    </div>

                </article>
                <article class="col-2">
                    <h3>Get In Touch</h3>
                    <br><br><br><br><br><br><br><br>

                    <div class="container">
                        <td>الوصف : {{$bid->piece->discription}}</td><br>
                        <td>القياس : {{$bid->piece->size}} </td><br>
                        <td>الوقت المتبقي :</td>
                        <div class="time">
                            <p id="demo"></p>

                            <script>
                                // Set the date we're counting down to
                                var countDownDate = new Date("{{$month}} {{$day}}, {{$year}} {{$hour}}: {{$minute}}: {{$second}}").getTime();

                                // Update the count down every 1 second
                                var x = setInterval(function() {

                                    // Get today's date and time
                                    var now = new Date().getTime();

                                    // Find the distance between now and the count down date
                                    var distance = countDownDate - now ;

                                    // Time calculations for days, hours, minutes and seconds
                                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                    // Display the result in the element with id="demo"
                                    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                                        + minutes + "m " + seconds + "s ";

                                    // If the count down is finished, write some text
                                    if (distance < 0) {
                                        clearInterval(x);
                                        document.getElementById("demo").innerHTML = "EXPIRED";
                                    }
                                }, 1000);
                            </script>
                            {{--                    @endif--}}
                        </div>
                        <div class="time2">
                            <p id="demo2"></p>
                        </div>
                        <p>الرقم الذي وصل له المزاد حتى الان : </p>
                        <div class="bidding">
                            <p>{{$bid->bid_amount}}</p>
                        </div>
                        <div class="bb">
                            @if($bid->user != null)
                                <p> قام  {{$bid->bidder}} بالمزايدة   </p>
                            @endif
                        </div>
                        <div class="nn">
                            @if($bid->user != null)
                                <p> في تاريخ : {{$bid_date}}</p><br>
                                <p> في تمام الساعة : {{$bid_time}}</p>
                            @endif
                        </div>
                        <form method="post" action="{{ route('bid.update', $bid->id) }}" enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="form-group">
                                <label for="full_name"> مقدار الزيادة:*</label>
                                <input type="number" class="form-control" name="value" id="chatu"/>
                            </div>
                            {{--                    <input type="hidden" class="form-control" name="id" value="{{$bid->id}}"/>--}}
                            <button type="submit" class="btn btn-primary">Add Bid</button>
                        </form>
                        <script src="{{ asset('js/app.js') }}"></script>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">Cufon.now();</script>
</body>
</html>
