{{--@extends('base')--}}

{{--@section('main')--}}
{{--    <div class="row">--}}
{{--        <x-app-layout>--}}

{{--            <div class="col-sm-11 offset-sm-2">--}}
{{--                <h1 class="display-2">Welcom to our Bidding website</h1>--}}
{{--                <div>--}}
{{--                    @if ($errors->any())--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            <ul>--}}
{{--                                @foreach ($errors->all() as $error)--}}
{{--                                    <li>{{ $error }}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div><br />--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--                <div>--}}
{{--                    <a href="{{route('bid.index')}}">Show Current Bid</a><br>--}}
{{--                    <a href="{{ route('bid.create')}}">Add New Bid</a><br>--}}
{{--                    <a href="{{ route('notif')}}">Send notification</a>--}}

{{--                    <script srs="{{asset('js/app.js')}}"></script>--}}
{{--                    <script src="js/app.js"></script>--}}
{{--                    <script>--}}
{{--                        window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{--}}
{{--                            console.log(e.result.content)--}}
{{--                        })--}}
{{--                    </script>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--        </x-app-layout>--}}
{{--        <br>--}}

{{--    </div>--}}
{{--@endsection--}}
@extends('bid.comm')

@section('main')
<body id="page1">
<!--==============================header=================================-->
<header>
    <!-- start top -->
    <div id="topnav" class="navbar navbar-fixed-top default">
        <div class="navbar-inner">
            <div class="container">
{{--                <div class="logo">--}}
{{--                    <a href="index.html"><img src="assets/img/logo.png" alt="" /></a>--}}
{{--                </div>--}}
                <div class="navigation">
                    <nav>
                        <ul class="nav pull-right">
                            <li class="current"><a href="/dashboard">الصفحة الرئيسية</a></li>

                            <li><a href="{{route('bid.index')}}">المزاد الحالي</a></li>
                            <li ><a class="noti" href="{{ route('notif')}}"><p>الإشعارات{{auth()->user()->unreadNotifications->count()}}</p></a></li>
                            @if(auth()->id()==1)
                                <li><a href="{{ route('bid.create')}}">إضافة مزاد</a></li>
                            @endif
                            <script srs="{{asset('js/app.js')}}"></script>
                            <script src="js/app.js"></script>
                            <script>
                                window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{
                                    console.log(e.result.content)
                                    document.getElementsByClassName('noti')[0].innerHTML='<p>'+'الإشعارات'+e.result.content+'</p>';
                                })
                            </script>
{{--                            <li><a href="#contact">Contact</a></li>--}}
{{--                            <li class="dropdown">--}}
{{--                                <a href="#">Features <i class="icon-angle-down"></i></a>--}}
{{--                                <ul class="dropdown-menu">--}}
{{--                                    <li><a href="components.html" class="external">Components</a></li>--}}
{{--                                    <li><a href="icons.html" class="external">Icons</a></li>--}}
{{--                                    <li><a href="animations.html" class="external">56 Animations</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
                            <li>
                                <form method="POST" action="{{ route('logout') }}" x-data>
                                    @csrf
                                    <button href="{{ route('logout') }}"
                                            @click.prevent="$root.submit();">
                                        تسجيل الخروج
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- end top -->
</header>
<!--==============================content================================-->
<section id="content">
    <div class="main">
        <br><br><br><br><br><br><br><br><br><br>
        <div class="slider-wrapper">
            <div class="slider">
                <ul class="items">
                    <li> <img src="images/slider-img1.jpg" alt="" /> <strong class="banner"> <a class="close" href="#">x</a> <strong>Progress</strong> <span>Business Company</span> <b class="margin-bot">Lorem ipsum dolor sit amet, consectetur adipisicing elitsedo eiusmod tempor incididunt ut labore dolore magna aliqua enim ad minim veniam.</b> <a class="button2" href="#">Read More</a> </strong> </li>
                    <li> <img src="images/slider-img2.jpg" alt="" /> <strong class="banner"> <a class="close" href="#">x</a> <strong>SpaSalon</strong> <span>popular Procedures</span> <b class="margin-bot">Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit<br>
                                in voluptate velit.</b> <a class="button2" href="#">Read More</a> </strong> </li>
                    <li> <img src="images/slider-img3.jpg" alt="" /> <strong class="banner"> <a class="close" href="#">x</a> <strong>GoodCook</strong> <span>Online cooking recipes</span> <b class="margin-bot">Esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia <br>
                                deserunt mollit anim.</b> <a class="button2" href="#">Read More</a> </strong> </li>
                    <li> <img src="images/slider-img4.jpg" alt="" /> <strong class="banner"> <a class="close" href="#">x</a> <strong>HandyMan</strong> <span>Home Services</span> <b class="margin-bot">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas.</b> <a class="button2" href="#">Read More</a> </strong> </li>
                </ul>
            </div>
            <ul class="pagination">
                <li><a class="item-1" href="#"><strong>01</strong></a></li>
                <li><a class="item-2" href="#"><strong>02</strong></a></li>
                <li><a class="item-3" href="#"><strong>03</strong></a></li>
                <li><a class="item-4" href="#"><strong>04</strong></a></li>
            </ul>
        </div>
        <div class="border-bot1 img-indent-bot">
            <p style="font-size:6vw">المزادات </p>
        </div>


        <div class="indent-left">

            <div class="wrapper margin-bot">
                @foreach($pieces as $piece)
                    <article class="col-1">
                        <div class="prev-indent-bot">
                            <figure class="img-border"><a href="#"><img src="{{$piece->piece_img}}" height="400" width="400" alt="" /></a></figure>
                            <div class="clear"></div>
                        </div>
                        <h6>قياس : {{$piece->size}}</h6>
                        <h6>من تاريخ  : {{$piece->start_date}}</h6>
                        <h6>إلى تاريخ : {{$piece->exp_date}}</h6>
                        <h6>حول : {{$piece->discription}}</h6>
                        @if($piece->winner != null)
                             <h6>الفائز بالمزاد : {{$piece->winner->name}}</h6>
                        @else
                            <h6>الفائز بالمزاد : لم ينتهي المزاد بعد</h6>
                        @endif

{{--                        <p class="p2">قئللقلسث</p>--}}
{{--                        <a class="button" href="#">Read قئللقلسث</a>--}}
                    </article>
                @endforeach




            </div>
        </div>
    </div>
</section>
<!--==============================footer=================================-->
{{--<footer>--}}
{{--    <div class="main">--}}
{{--        <div class="wrapper border-bot2 margin-bot">--}}
{{--            <article class="fcol-1">--}}
{{--                <div class="indent-left">--}}
{{--                    <h3 class="color-1">Stay Connected</h3>--}}
{{--                    <ul class="list-services">--}}
{{--                        <li><a href="#">Facebook</a></li>--}}
{{--                        <li><a class="it-2" href="#">Twitter</a></li>--}}
{{--                        <li><a class="it-3" href="#">Linked In</a></li>--}}
{{--                        <li class="last-item"><a class="it-4" href="#">Del.ico.us</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </article>--}}
{{--            <article class="fcol-2">--}}
{{--                <h3 class="color-1">Strategy Solution</h3>--}}
{{--                <p class="p3">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore fugiat pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit velit esse.</p>--}}
{{--                <a class="button2" href="#">Read More</a> </article>--}}
{{--            <article class="fcol-3">--}}
{{--                <h3 class="color-1">Links</h3>--}}
{{--                <ul class="list-3">--}}
{{--                    <li><a href="#">Flash Resources</a> <span> - Lorem ipsum dolor</span></li>--}}
{{--                    <li><a href="#">CSS &amp; Coding Tutorials</a> <span> - Excepteur sint</span></li>--}}
{{--                    <li><a href="#">Free Clipart for Design</a> <span> - Duis autre dolor</span></li>--}}
{{--                    <li class="last-item"><a href="#">Design Education</a> <span> - Neque quisquam</span></li>--}}
{{--                </ul>--}}
{{--            </article>--}}
{{--        </div>--}}
{{--        <div class="aligncenter">Copyright &copy; <a class="color-1" href="#">Domain Name</a> All Rights Reserved<br>--}}
{{--            Design by <a target="_blank" href="http://www.templatemonster.com/" class="color-1">TemplateMonster.com</a> </div>--}}
{{--    </div>--}}
{{--</footer>--}}
<script type="text/javascript">Cufon.now();</script>
<script type="text/javascript">
    $(window).load(function () {
        $('.slider')._TMS({
            duration: 800,
            easing: 'easeOutQuart',
            preset: 'diagonalExpand',
            slideshow: 7000,
            pagNums: false,
            pagination: '.pagination',
            banners: 'fade',
            pauseOnHover: true,
            waitBannerAnimation: true
        });
    });
</script>
</body>
@endsection
