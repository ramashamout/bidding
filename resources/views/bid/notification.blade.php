{{--@extends('base')--}}
{{--@section('main')--}}
{{--    <div class="row">--}}
{{--        <div class="col-sm-12">--}}
{{--            <h1 class="display-3">Bid Notification</h1>--}}
{{--            <div>--}}
{{--                <a href="{{ route('brideReminder.create')}}" class="btn btn-primary mb-3">Add bride Notification</a>--}}
{{--            </div>--}}

{{--            @if(session()->get('success'))--}}
{{--                <div class="alert alert-success">--}}
{{--                    {{ session()->get('success') }}--}}
{{--                </div>--}}
{{--            @endif--}}
{{--            <table class="table table-striped">--}}
{{--                <thead>--}}
{{--                <tr>--}}
{{--                    <td>ID</td>--}}
{{--                    <td> content</td>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                <h1>Bride </h1>--}}
{{--                @foreach($user->unreadNotifications as $brideReminder)--}}
{{--                    @if($brideReminder->data['type'] =="bride")--}}
{{--                        <tr>--}}
{{--                            <td>{{$brideReminder->data['id']}}</td>--}}
{{--                            <td>{{$brideReminder->data['content']}} </td>--}}
{{--                            <td>{{$brideReminder->data['date']}}</td>--}}
{{--                            <td>{{$brideReminder->data['mobile_number']}}</td>--}}
{{--                            <td>{{$brideReminder->data['emboridery']}}</td>--}}
{{--                            <td><img src="{{$brideReminder->data['image']}}"></td>image--}}
{{--                            <td>--}}
{{--                                <a href="{{ route('markAsRead',$brideReminder->id)}}" class="btn btn-primary">mark as read</a>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--            <div>--}}
{{--            </div></div></div>--}}
{{--@endsection--}}

@extends('bid.comm')

@section('main')
<body id="page5">
<!--==============================header=================================-->
<header>
    <div id="topnav" class="navbar navbar-fixed-top default">
        <div class="navbar-inner">
            <div class="container">
                <div class="logo">
                    {{--                    <a href="index.html"><img src="assets/img/logo.png" alt="" /></a>--}}
                    @if(Auth::user()!=null)
                        {{--                        <h1>current user</h1>--}}
                        <div>{{Auth::user()->name}}</div><br>
                    @endif
                </div>
                <div class="navigation">
                    <nav>
                        <ul class="nav pull-right">
                            <li ><a href="/dashboard">الصفحة الرئيسية</a></li>
                            <li ><a href="{{route('bid.index')}}">المزاد الحالي</a></li>
                            <li class="current"><a class="noti" href="{{ route('notif')}}"><p>الإشعارات{{auth()->user()->unreadNotifications->count()}}</p></a></li>
                            @if(auth()->id()==1)
                                <li><a href="{{ route('bid.create')}}">إضافة مزاد</a></li>
                            @endif
                            <script srs="{{asset('js/app.js')}}"></script>
                            <script src="js/app.js"></script>
                            <script>
                                window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{
                                    console.log(e.result.content)
                                    document.getElementsByClassName('noti')[0].innerHTML='<p>'+'الإشعارات'+e.result.content+'</p>';
                                })
                            </script>
                            <li>
                                <form method="POST" action="{{ route('logout') }}" x-data>
                                    @csrf
                                    <button href="{{ route('logout') }}"
                                            @click.prevent="$root.submit();">
                                        تسجيل الخروج
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- end top -->
</header>
<!--==============================content================================-->
<section id="content">
    <div class="main">
        <div class="indent-left">
            <div class="wrapper">
                <article class="col-1">
                    <br><br><br><br><br><br><br><br><br><br><br><br>
                    <div>


                    </div>

                </article>
                <article class="col-2">
                    <h3>Get In Touch</h3>
                    <br><br><br><br><br><br><br><br>

                    <div class="container">
                        @foreach($user->unreadNotifications as $brideReminder)
                        <tr>
                            <td><img src="{{$brideReminder->data['image']}}" height="300"width="300"></td>

                            <td>{{$brideReminder->data['content']}} </td>
                            <td>
                                <a href="{{ route('markAsRead',$brideReminder->id)}}" class="btn btn-primary">تعليم كمقروء</a>
                            </td>


                            <br><br>


                        </tr>

                        @endforeach


                        <script src="{{ asset('js/app.js') }}"></script>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">Cufon.now();</script>
</body>
@endsection
