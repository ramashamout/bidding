


{{--@extends('base')--}}

{{--@section('main')--}}
{{--    <div class="row">--}}
{{--        <div class="col-sm-8 offset-sm-2">--}}
{{--            <h1 class="display-3">Add New Bid</h1>--}}
{{--            <div>--}}
{{--                @if ($errors->any())--}}
{{--                    <div class="alert alert-danger">--}}
{{--                        <ul>--}}
{{--                            @foreach ($errors->all() as $error)--}}
{{--                                <li>{{ $error }}</li>--}}
{{--                            @endforeach--}}
{{--                        </ul>--}}
{{--                    </div><br />--}}
{{--                @endif--}}
{{--                <form method="post" action="{{ route('bid.store') }}" enctype="multipart/form-data">--}}
{{--                    @csrf--}}
{{--                    <div class="form-group">--}}
{{--                        <label > discription:*</label>--}}
{{--                        <input type="text" class="form-control" name="discription"/>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label > piece_img:*</label>--}}
{{--                        <input type="file" class="form-control" name="piece_img"/>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="value"> statr Bid at value:</label>--}}
{{--                        <input type="number" class="form-control" name="bidding_price"/>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="value"> expecting price:</label>--}}
{{--                        <input type="number" class="form-control" name="expecting_price"/>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label > size:</label>--}}
{{--                        <input type="number" class="form-control" name="size"/>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label > start Date:</label>--}}
{{--                        <input type="datetime-local" class="form-control" name="start_date"/>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label > Exp Date:</label>--}}
{{--                        <input type="datetime-local" class="form-control" name="exp_date"/>--}}
{{--                    </div>--}}
{{--                    <button type="submit" class="btn btn-primary">Add Piece to bidding</button>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
{{--@extends('bid.comm')--}}

{{--@section('main')--}}
<body id="page1">
<!--==============================header=================================-->
<header>
    <!-- start top -->
    <div id="topnav" class="navbar navbar-fixed-top default">
        <div class="navbar-inner">
            <div class="container">
                {{--                <div class="logo">--}}
                {{--                    <a href="index.html"><img src="assets/img/logo.png" alt="" /></a>--}}
                {{--                </div>--}}
                <div class="navigation">
                    <nav>
                        <ul class="nav pull-right">
                            <li ><a href="/dashboard">الصفحة الرئيسية</a></li>

                            <li><a href="{{route('bid.index')}}">المزاد الحالي</a></li>
                            <li ><a class="noti" href="{{ route('notif')}}"><p>الإشعارات{{auth()->user()->unreadNotifications->count()}}</p></a></li>
                            @if(auth()->id()==1)
                                <li class="current"><a href="{{ route('bid.create')}}">إضافة مزاد</a></li>
                            @endif
                            <script srs="{{asset('js/app.js')}}"></script>
                            <script src="js/app.js"></script>
                            <script>
                                window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{
                                    console.log(e.result.content)
                                    document.getElementsByClassName('noti')[0].innerHTML='<p>'+'الإشعارات'+e.result.content+'</p>';
                                })
                            </script>
                            <li>
                                <form method="POST" action="{{ route('logout') }}" x-data>
                                    @csrf
                                    <button href="{{ route('logout') }}"
                                            @click.prevent="$root.submit();">
                                        تسجيل الخروج
                                    </button>
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>

                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- end top -->
</header>
<!--==============================content================================-->
<section id="content">
    <div class="main">
        <div class="indent-right">
            <div class="wrapper">
                <article >
                    <br><br><br><br><br><br><br><br>
                    <h3>إضافة مزاد</h3>
                    <form id="contact-form"  method="post" action="{{ route('bid.store') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">

                                            <input type="text" class="form-control" name="discription" placeholder="حول القطعة"/><br>
                                        </div><br>
                                        <div class="form-group ">

{{--                                            <input type="file" class="form-control" name="piece_img" placeholder="صور القطعة"/><br><br>--}}
                                            <input required type="file" class="form-control" name="images[]" placeholder="صور القطعة" multiple><br>
                                        </div><br>
                                        <div class="form-group">

                                            <input type="number" class="form-control" name="bidding_price" placeholder="السعر الابتدائي"/><br>
                                        </div><br>
                                        <div class="form-group">

                                            <input type="number" class="form-control" name="expecting_price" placeholder="السعر المتوقع"/><br>
                                        </div><br>
                                        <div class="form-group">

                                            <input type="number" class="form-control" name="added_value" placeholder="القياس"/><br>
                                        </div><br>
                                        <div class="form-group">

                                            <input type="datetime-local" class="form-control" name="start_date" placeholder="تاريخ البداية"/><br>
                                        </div><br>
                                        <div class="form-group">

                                            <input type="datetime-local" class="form-control" name="exp_date" placeholder="تاريخ النهاية"/>
                                        </div><br>
                                        <button type="submit" class="btn btn-primary">إضافة</button>
                                    </form>

                </article>
            </div>
        </div>
    </div>
</section>
<!--==============================footer=================================-->

<script type="text/javascript">Cufon.now();</script>
</body>
{{--@endsection--}}
