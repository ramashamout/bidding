<!DOCTYPE html>
<html lang="ar-sa" dir="rtl">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <meta content="" name="description">
      <meta content="" name="keywords">
     <!--select accessory css -->
      <link rel="stylesheet" type="text/css" href="/css/bootstrap-select.css"> 
      <!-- Favicons -->
      <link href="/assets/img/favicon.png" rel="icon">
      <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
      <!-- Fonts -->
      <link rel="stylesheet" type="text/css" href="...../fonts/DroidKufi/EARLY_ACCESS.css">
      <link rel="stylesheet" type="text/css" href="/fonts/AlexBrush/AlexBrush-Regular.ttf">
      <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
        <!--select accessory css -->
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
      <link rel="stylesheet" type="text/css" href=".../css/bootstrap-select.css">
     
      <!-- Vendor CSS Files -->
      <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
      <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
      <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
      <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
      <link href="/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
      <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
      <!-- Template Main CSS File -->
      <link href="/assets/css/style.css" rel="stylesheet">

   </head>
   <body>

      <!-- ======= Header ======= -->
      <header id="header" class="fixed-top "  style="background: rgba(40, 58, 90, 0.9);">
         <div class="container d-flex align-items-center" >
            <nav id="navbar" class="navbar ms-auto " >
              <ul>
 
           <li><a class="nav-link scrollto " href="/index">الرئيسية</a></li>
            <li><a class="nav-link scrollto" href="/actions">المزادات</a></li>
            <li><a class="nav-link scrollto" href="{{route('main_s.index')}}">الأقسام</a></li>

            @auth

                @hasanyrole($roles)
           <li class="dropdown"><a href="#"><span>لوحة التحكم</span> <i class="bi bi-chevron-down"></i></a>
            <ul>


           @can('إدارة المستخدمين ')
            <li class="dropdown"><a ><span><i class="bi bi-chevron-right"></i>إدارة المستخدمين </span> </a>

                <ul>
             

                  <li><a href="/control_users">المستخدمين </a></li>
                  <li><a href="/control_users_role">المشرفين </a></li>
                  <li><a href="/roles/create">إدارة الصلاحيات</a> </li>
                </ul>
            </li>
            @endcan
            @can('إدارة الأقسام ')
               <li><a href="/all_section">إدارة  الأقسام</a></li>
            @endcan
            @can('إدارة التصنيفات ')
               <li><a href="/all_category">إدارة  التصنيفات</a></li>
            @endcan
            @can('إدارة المزادات  ')

              <li class="dropdown"><a ><span><i class="bi bi-chevron-right"></i>إدراة المزادات</span> </a>

                <ul>
                    @foreach($all_sections as $all_sections)

                  <li><a href="{{route('control_action',$all_sections->id)}}">{{$all_sections->name}}</a></li>
                  @endforeach

                </ul>

              </li>
             @endcan
            </ul>
          </li>
           @endhasanyrole
             <li><a class="getstarted scrollto" > {{auth()->user()->name}}</a></li>
                  <li>
             <form method="POST" action="{{ route('logout') }}" style="font-size:16px;">
                  @csrf
            <a class="getstarted scrollto" href="{{ route('logout') }}" onclick="event.preventDefault();
                        this.closest('form').submit(); " role="button">تسجيل الخروج </a>

            </form></li>
              
           @else
           <li><a class="getstarted scrollto  " href="/login" style="font-size:16px;">تسجيل الدخول</a></li>
           <li><a class="getstarted scrollto  " href="/register" style="font-size:16px;">إنشاء حساب</a></li>
           @endauth

        </ul>
               <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- .navbar -->
         </div>
      </header>
      <!-- End Header -->
    @yield('contents')

      <script type="text/javascript" src="/js/jquery.min.js"></script>
     <script type="text/javascript" src="/js/bootstrap.bundle.min.js"></script>
     <script type="text/javascript" src="/js/bootstrap-select.min.js"></script>
      <!-- End Why Us Section -->
      <script src="/assets/vendor/aos/aos.js"></script>
      <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
      <script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
      <script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
      <script src="/assets/vendor/waypoints/noframework.waypoints.js"></script>
      <script src="/assets/vendor/php-email-form/validate.js"></script>
      <!-- Template Main JS File -->
      <script src="/assets/js/main.js"></script>

   </body>
</html>
