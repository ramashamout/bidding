@extends('master') @section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="assets/img/old.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
  </section><!-- End Hero -->
  <!-- ======= Team Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">
      <div class="section-title">
        <h2>المزاد المباشر</h2>
        <p>نرحب بك للانضمام في هذا المزاد.</p>
      </div>
      <div class="row">
        <!-- start product gallery -->
        <div class=" col-lg-5 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200" >
          <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner" >
              <div class="carousel-item active">
                <img src="{{$bid->piece->images[0]->piece_img}}" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="{{$bid->piece->images[1]->piece_img}}" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="{{$bid->piece->images[2]->piece_img}}" class="d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>
        <!-- end product gallery -->
        <!-- start  details and calendar-->
        <div class=" col-lg-7 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <div class="card"  style="border-radius: 10px 10px 10px 10px;border-color: #C8C8C8; margin-bottom:220px;">
            <div class="card-body">
              <a  href="mail_compose.html" class="btn btn-compose">
              <i class="fa fa-pencil"></i>معلومات المزايدة</a>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                <div class="team ">
                  <div class="member d-flex align-items-start"  >
                    <img src="assets/img/desc2.jpg" class="rounded-circle" alt=""  style="width: 50px;  margin-top: 15px;">
                    <div class="member-info ">
                      <h4 > الوصف </h4>
                      <span>
                      {{$bid->piece->discription}} </span>
                    </div>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="member d-flex align-items-start"  >
                      <img src="assets/img/minus.jpg" class="rounded-circle" alt=""  style="width: 50px;  margin-top: 15px;">
                      <div class="member-info">
                        <h4 >السعر الإبتدائي</h4>
                        <h3 id="price" style="font-size: 20px;margin-right: 15px; ">{{$bid->piece->bidding_price}} SYR</h3>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="team ">
                      <div class="member d-flex align-items-start"  >
                        <img src="assets/img/time.jpg" class="rounded-circle" alt=""  style="width: 50px; margin-top: 15px;">
                        <div class="member-info">
                          <h4 > الوقت المتبقي</h4>
                          <span id="demo" style="font-size: 22px;color:green;"></span>
                          <span id="demo2" style="font-size: 22px;color:green;"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="team ">
                      <div class="member d-flex align-items-start"  >
                        <img src="assets/img/push2.jpg" class="rounded-circle" alt=""  style="width: 50px; margin-top: 15px;">
                         @auth
                                @hasanyrole($roles)
                              <div class="member-info">
                          <h4>قيمة المزايدة </h4>
                          <h3 id="price" class="bidding" style="font-size: 22px;margin-right: 25px;">
                         {{$bid->piece->added_value}}</h3>
                        </div>
                                 @else
                        <div class="member-info">
                          <h4> شارك بالمزاد</h4>
                          <form  method="post" action="{{ route('bid.update', $bid->id) }}"  >
                            @method('PATCH')
                            @csrf
                            <div class="row p">
                              <div class="input-group">
                                <div class = "col-md-8  col-8" >
                                  <input  type="number" name="value"  value="{{$bid->piece->added_value}}" step="{{$bid->piece->added_value}}" min="{{$bid->piece->added_value}}" max="1000000"  required >
                               
                                  <input type="hidden" class="form-control" name="id" value="{{$bid->id}}"/>
                                  <span class="validity" ></span>
                                </div>
                               
                                <div class="col-md-4  col-4">
                                  <span class="input-group-append">
                                    <button  class=" btn-secondary  btn-xs"style="border-radius: 5px;"  type="submit">
                                    <i class="bi bi-plus"></i>
                                    </button>
                                  </span>
                                </div>
                               
                              </div>
                            </div>
                            <!-- error mesage -->
                            
                                @if ($errors!=null)
                                <div class="alert alert-danger">
                                  <ul>
                                    <li>
                                      {{ $errors }}
                                    </li>
                                  </ul>
                                </div><br />
                                @endif
                              
                          </form>
                        </div>
                          @endhasanyrole
                        @endauth
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="team ">
                      <div class="member d-flex align-items-start"  >
                        <img src="assets/img/money.jpg" class="rounded-circle" alt=""  style="width: 50px;  margin-top: 15px;">
                        <div class="member-info">
                          <h4> السعر الحالي</h4>
                          <h3 id="price" class="bidding" style="font-size: 22px;margin-right: 15px;">{{$bid->bid_amount}} SYR</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="team ">
                  <div class="member d-flex align-items-start"  >
                    <img src="assets/img/bid1.jpg" class="rounded-circle" alt=""  style="width: 50px;  margin-top: 15px;">
                    <div class="member-info">
                      <h4> المزايد النهائي</h4>
                      @if($bid->user != null)
                      
                      <span class="bidder">قام {{$bid->bidder}} بالمزايدة في تاريخ  {{$bid_date}} تمام الساعة  {{$bid_time}}</span>
                      @else
                      <span>لم يزايد احد بعد</span>
                      @endif
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <script>
        // Set the date we're counting down to
        var countDownDate = new Date("{{$month}} {{$day}}, {{$year}} {{$hour}}: {{$minute}}: {{$second}}").getTime();
        // Update the count down every 1 second
        var x = setInterval(function() {
        // Get today's date and time
        var now = new Date().getTime();
        // Find the distance between now and the count down date
        var distance = countDownDate - now ;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Display the result in the element with id="demo"
        document.getElementById("demo").innerHTML = days + "d " + hours + "h "
        + minutes + "m " + seconds + "s ";
        // If the count down is finished, write some text
        if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
        }
        }, 1000);
        </script>
        <!-- end details and calendar-->
      </div>
    </div>
    </section><!-- End Team Section -->
    @endsection