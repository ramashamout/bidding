@extends('master') @section('contents')
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">

        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
                    <h1> مزادات مغرية  من أجلك </h1>
                    <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>

                </div>
                <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
                    <img src="/assets/img/banner-2.png" class="img-fluid animated" alt="">
                </div>
            </div>
        </div>

    </section><!-- End Hero -->
    <section id="section" class="section section-bg">
        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h3 >التصنيفات</h3>
                <p>اختر التصنيف المناسب</p>
            </header>

            <div class="row g-5">

               



                @foreach($main_s->sub_sections as $main_s)


                          <div class="col-md-2 col-lg-2 col-4 line" data-aos="zoom-in" data-aos-delay="300">
                            <div class="box">
                             <img src="{{$main_s->sub_s_img}}" class="img-fluid" alt="">
                              <h4 class="title"><a href="/sub_s_p/{{$main_s->id}}">{{$main_s->name}}</a></h4>
                              </div>
                          </div>

                  @endforeach








            </div>


        </div>
    </section>

@endsection
