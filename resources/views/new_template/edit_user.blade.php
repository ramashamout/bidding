@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="/assets/img/usercontrol.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>تعديل المستخدم </h2>
      
    </div>
    <div class="row mt justify-content-center">
      <div class="col-lg-8 " data-aos="fade-up" data-aos-delay="200">
        <div class="content-panel">
        
               {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id],'class'=>'display']) !!}
            <div class="row">
              
             
                  <label>الصلاحيات :</label>
                 
               
              @foreach($roles as $role)
                  <div class="col-lg-3 col-md-3 col-3">
                <div class="text-center " style="  padding: 30px;">
                  
                   
                   {{ Form::checkbox('roles[]', 
                   $role['id'],
                    in_array($role['id'], $userRoles) ? true : false, array('class' => 'name')) }} 
                    {{ $role->name }}
                   </div>
              </div>
                    @endforeach
            </div>
            <div class="text-center">
              <button  class="close " type="submit">تعديل </button>
            </div>
           {!! Form::close() !!}
          <div>
            <!-- <label for="name">Assigned Permissions</label> -->
            
            
          </div>
        </div>
        <!-- /content-panel -->
      </div>
      <!-- /col-md-12 -->
    </div>
  </div>
</section>
@endsection