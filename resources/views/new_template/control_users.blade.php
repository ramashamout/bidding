@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="assets/img/usercontrol.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>إدارة  المستخدمين</h2>
      
    </div>
    <div class="row mt justify-content-center">
      <div class="col-lg-8 " data-aos="fade-up" data-aos-delay="200">
        <div class="content-panel">
          <table class="table table-striped table-advance table-hover">
            
            <hr>
            <thead>
              <tr>
                <th class="text-center"><i class="bi bi-person-fill" ></i> المستخدمين </th>
                
                <th class="text-center">البريد الإلكتروني </th>
                <th class="text-center">تعديل صلاحية</th>
                <th class="text-center"><i class=" bi bi-slash-circle" style="color: red;"></i> Block </th>
               
                
              </tr>
            </thead>
            <tbody>
              
              
              @foreach ($data as $key => $user)
             <?php

$userRoles = DB::Connection('mysql2')->table("model_has_roles")->where("model_has_roles.model_id",$user->id)
            ->pluck('model_has_roles.role_id','model_has_roles.role_id')
            ->all();
?>
@if(!$userRoles)
              <tr>
                <td class="text-center">
                  
                  <h6 style=" color: #37517e;">{{$user->name}}</h6>
                  
                </td>
                
                
                <td class="text-center">
                  {{$user->email}}
                </td>
                
               <!--  <td class="text-center">
                  @if(!empty($user->getRoleNames()))
                  @foreach($user->getRoleNames() as $v)
                  <h6 style=" color: #37517e;">{{ $v }}</h6>
                  @endforeach
                  
                  @else
                  <h6 style=" color: #37517e;">لا يوجد </h6>
                  @endif
                </td> -->
                
                <td class="text-center">
                  <a  class="btn btn-primary btn-xs"  href="{{ route('users.edit', $user->id) }}">
                    <i class=" bi bi-pencil  btn-xs"></i>
                  </a>
                  
                  
                </td>
                <td class="text-center">
                  @if($user->status==1)
                  <a class="btn btn-success btn-xs"  href="{{ route('block', $user->id) }}">
                    <i class=" bi bi-slash-circle  btn-xs"></i>
                  </a>
                  @else
                  <a class="btn btn-danger btn-xs"  href="{{ route('unblock', $user->id) }}">
                    <i class=" bi bi-slash-circle  btn-xs"></i>
                  </a>
                  @endif
                </td>
                
              </tr>
              @endif
              @endforeach
              
              
            </tbody>
          </table>
        </div>
        <!-- /content-panel -->
      </div>
      <!-- /col-md-12 -->
    </div>
  </div>
</section>
@endsection