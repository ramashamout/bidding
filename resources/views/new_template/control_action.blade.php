@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="/assets/img/dashboard.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>إدارة المزادات</h2>
    </div>
    <ul class="nav nav-pills mb-3 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100"role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">إضافة مزاد</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">المزادات   الماضية</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">المزادات القادمة</button>
      </li>
    </ul>
    <div class="row tab-content" data-aos="fade-up" data-aos-delay="200">
      <!-- add action -->
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="row">
          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form id="contact-form"  method="post" action="{{ route('bid.store') }}" enctype="multipart/form-data" class="display">
              @csrf
              <div class="row">
                <div class="form-group ">
                  <select name="sub_section_id" class="form-control">
                  
                    @foreach($current_s->sub_sections as $main_s)
                    <option value="{{$main_s->id}}">{{$main_s->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-6">
                  <input type="text" name="discription" class="form-control" id="name" placeholder ="الوصف" required style="margin-top: 15px;">
               
                </div>
                <div class="form-group col-md-6">
                  <input type="number" name="bidding_price" class="form-control" id="name" placeholder ="السعر الإبتدائي" required style="margin-top: 15px;">
                
                </div>
             
                <div class="form-group col-md-6">
                  <input type="number" name="added_value" class="form-control" id="name" required placeholder="قيمة المزايدة" style="margin-top: 15px;">
                </div>
                <div class="form-group col-md-6">
                  <input type="text" name="start_date" input type="text" class="form-control" onfocus="(this.type='datetime-local')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ البدء" style="margin-top: 15px;">
                
                </div>
                <div class="form-group col-md-6">
                  <input type="text" input type="text" class="form-control" onfocus="(this.type='datetime-local')" onblur="if(!this.value)this.type='text'" name="exp_date" id="email" required placeholder="تاريخ الإنتهاء" style="margin-top: 15px;" >
                
                </div>
                <div class="form-group  " style="  padding: 30px;">
                  <div class=" fileUpload btn btn-light">
                    <label class="upload">
                    <input  name="images[]"  type="file" class="form-control mx-sm-3" multiple required>تحميل الصور </label>
                  </div>
                </div>
              </div>
              <div class="text-center"><button type="submit">حفظ</button></div>
             <br>
                  @if ($errors!=null)
                  <div class="alert alert-danger">
                    <ul>
                      <li>
                        {{ $errors }}
                      </li>
                    </ul>
                  </div><br />
                  @endif
             
            </form>
          </div>
        </div>
      </div>
      <!-- all action -->
      <div class="tab-pane fade " id="pills-contact" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="row">
          <section class="task-panel tasks-widget"style="border-top: 3px solid #47b2e4;">
            @foreach($old_piece as $old_piece)
            <div class="col-md-12 col-lg-12 col-12 title_action" >
              <h5> <img class="dashhammer1" src="/assets/img/section.jpg">{{$old_piece->sub_section->name}}</h5>
            </div>
            <div class="col-md-12 col-lg-12 col-12">
              <div class="task-content">
                <ul class="task-list">
                  <!--comming bid -->
                  <li>
                    <div class="task-title">
                      <span class="task-title-sp">
                        <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed">
                          <img class="dashhammer1" src="/assets/img/images.png">
                          &nbsp;{{$old_piece->discription}}&nbsp;
                          <button class=" btn-primary btn-xs dashbtn">
                        <i class=" bi bi-eye"></i></button></a>
                        <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                          <div class="custom-check goleft mt">
                            <table id="todo" class="table table-hover custom-check">
                              <tbody>
                                <tr>
                                  <td>
                                    <div class="row">
                                      <div class="col-md-1 col-lg-1 col-3" style=" margin-bottom: 5px;">
                                        <span>الصورة :</span>
                                      </div>
                                      <div class="col-md-1 col-lg-1 col-3">
                                        <img class="dashpic" src="{{$old_piece->images[0]->piece_img}}" style="margin-bottom: 5px;">
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>السعر النهائي :</span>
                                    {{$old_piece->bid->last()->bid_amount}}
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>تاريخ المزاد :</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                    من {{$old_piece->start_date}}  إلى {{$old_piece->exp_date}}
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>الفائز :</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    @if($old_piece->winner != null)
                                    {{$old_piece->winner->name}}
                                    @else
                                    لم ينتهي المزاد بعد
                                    @endif
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          {{--                          <button class=" btn-danger btn-xs dashbtn "><i class="bi bi-trash "></i></button>--}}
                          <form action="{{ route('bid.destroy',$old_piece->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-xs dashbtn" type="submit"><i class="bi bi-trash "></i></button>
                          </form>
                        </div>
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            @endforeach
          </section>
        </div>
      </div>
      <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <div class="row">
          <section class="task-panel tasks-widget"style="border-top: 3px solid #47b2e4;">
            @foreach($futur_piece as $old_piece)
            <div class="col-md-12 col-lg-12 col-12 title_action" >
              <h5> <img class="dashhammer1" src="/assets/img/section.jpg">{{$old_piece->sub_section->name}}</h5>
            </div>
            <div class="col-md-12 col-lg-12 col-12">
              <div class="task-content">
                <ul class="task-list">
                  <!--comming bid -->
                  <li>
                    <div class="task-title">
                      <span class="task-title-sp">
                        <a data-bs-toggle="collapse" href="#collapseExample{{$old_piece->id}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$old_piece->id}}">
                          <img class="dashhammer1" src="/assets/img/images.png">
                          &nbsp;&nbsp;{{$old_piece->discription}}&nbsp;&nbsp;
                          <button class=" btn-primary btn-xs dashbtn">
                        <i class=" bi bi-eye"></i></button></a>
                      <div class="collapse" id="collapseExample{{$old_piece->id}}">

                          <div class="custom-check goleft mt">
                            <table id="todo" class="table table-hover custom-check">
                              <tbody>
                                <tr>
                                  <td>
                                    <div class="row">
                                      <div class="col-md-1 col-lg-1 col-3" style=" margin-bottom: 5px;">
                                        <span>الصورة :</span>
                                      </div>
                                      <div class="col-md-1 col-lg-1 col-3">
                                        <img class="dashpic" src="{{$old_piece->images[0]->piece_img}}" style="margin-bottom: 5px;">
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>السعر الإبتدائي :</span>&nbsp;
                                    {{$old_piece->bidding_price}}
                                  </td>
                                </tr>
                              
                                <tr>
                                  <td>
                                    <span>قيمة المزايدة :</span>
                                    &nbsp;{{$old_piece->added_value}}
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>تاريخ البدء  :</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    {{$old_piece->start_date}}
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <span>تاريخ الانتهاء :</span>&nbsp;
                                    {{$old_piece->exp_date}}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <!-- eye modal -->
                          <div class="row ">
                            <div class="col-lg-1">
                              <a type="button" class=" btn-success btn-xs dashbtn" href="{{route('piece.edit',$old_piece->id)}}"style="  padding: 2px 5px 2px 5px;"><i class="bi bi-pencil" ></i></a></div>
                            <div class="col-lg-1">
                              
                         
                              <form action="{{ route('bid.destroy',$old_piece->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="  btn-danger btn-xs dashbtn"   href="{{ route('bid.destroy',$old_piece->id)}}" style="background: red; padding: 2px 5px 2px 5px; "><i class="bi bi-trash" ></i></button>
                          </form>
                            </div>
                          
                        
                        </div>
                        </div>
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            @endforeach
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection