@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="assets/img/dashboard.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>إدارة  الأقسام</h2>
      
    </div>
    <ul class="nav nav-pills mb-3 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100"role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">إضافة قسم</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">جميع الأقسام</button>
      </li>
      
    </ul>
    <div class="row tab-content" data-aos="fade-up" data-aos-delay="200">
      <!-- add action -->
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="row justify-content-center">
          
          <div class="col-lg-9 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="{{ route('main_s.store') }}" method="post" role="form" enctype="multipart/form-data"  class="display">
              @csrf
              <div class="row">
                
                <div class="form-group">
                  
                  <input type="text" name="name" class="form-control" id="name" required placeholder="إسم القسم">
                </div>
                
                
                
                <div class="form-group  " style="  padding: 30px;">
                  <div class=" fileUpload btn btn-light">
                    <label class="upload">
                      <input name='image' type="file" class="form-control ">تحميل صورة </label>
                    </div>
                  </div>
                  
                </div>
                
                
                <div class="text-center"><button type="submit">حفظ</button></div>
              </form>
            </div>
          </div>
        </div>
        <!-- all action -->
        <div class="tab-pane fade " id="pills-contact" role="tabpanel" aria-labelledby="pills-home-tab">
          
          <div class="row">
            
            
            <section class="task-panel tasks-widget"style="border-top: 3px solid #47b2e4;">
              
              
              <div class="col-md-12 col-lg-12">
                <div class="task-content">
                   @foreach($section as $section)
                  <ul class="task-list">
                    <!--comming bid -->
                    <li>
                     
                      <div class="task-title">
                        
                        
                        <span class="task-title-sp">
                          <img class="dashhammer" src="{{$section->main_s_img}}">
                          &nbsp;{{$section->name}} &nbsp;
                          <a data-bs-toggle="collapse" href="#collapseExample{{$section->id}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$section->id}}">
                            
                            <button class=" btn-success btn-xs dashbtn">
                          <i class=" bi bi-pencil"></i></button></a>
                          
                        
                          
                          <div class="collapse" id="collapseExample{{$section->id}}">
                           <div class="custom-check goleft mt">
                              <table id="todo" class="table table-hover custom-check">
                                <tbody>
                                  <tr>
                                    <td>
                                      <form method="post" action="{{route('main_s.update',$section->id)}}" enctype="multipart/form-data" >
                                        @method('PATCH')
                                        @csrf
                                        <div class="row">
                                          <div class="col-lg-6 col-md-6 col-6">
                                            <div class="form-group  " style="  padding: 30px;">
                                              
                                              <input type="text" name="name" class="form-control" id="name"
                                              value="{{$section->name}}"
                                              required>
                                            </div>
                                          </div>
                                          <div class="col-lg-6 col-md-6 col-6">
                                            <div class="form-group  " style="  padding: 30px;">
                                              <div class=" fileUpload btn btn-light">
                                                <label class="upload">
                                                  <input name='Image' type="file" class="form-control "
                                                  value="{{$section->main_s_img}}"
                                                >تحميل صورة </label>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="text-center">
                                          <button class="close " type="submit">حفظ</button>
                                            <form action="{{ route('main_s.destroy',$section->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class=" close"  href="{{ route('main_s.destroy',$section->id)}}" style="background: red;">حذف</button>
                          </form>
                          
                                        </div>
                                      </form>
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                      
                                    </td>
                                  </tr>
                                  
                                  
                                </tbody>
                              </table>
                            </div>
                            
                            
                            
                            
                          </div>
                        </span>
                        
                      </div>
                     
                    </li>
                    
                  </ul>
                   @endforeach
                </div>
              </div>
            </section>
          </div>
          
        </div>
        
        
      </div>
    </div>
  </section>
  @endsection