@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="/assets/img/dashboard.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<div class="row justify-content-center">
  <div class="col-lg-9 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
    <section id="contact" class="contact">
      <div  class="container"  >
        <!--  role="form" class="php-email-form" -->
        <form  method="POST" action="{{ route('add_from_bride',$piece_bride_id) }}" enctype="multipart/form-data" class="display">
          @csrf
          <div class="row d-flex  align-items-center">
            
            <div class="form-group ">
              <select name="sub_section_id" class="form-control" >
                @foreach(\App\Models\Main_section::find(5)->sub_sections as $sub_s)
                <option value="{{$sub_s->id}}">{{$sub_s->name}}</option>
                @endforeach
              </select>
            </div>
             <div class="form-group col-md-6">
                <input type="text" name="discription" class="form-control" id="name" value="{{$des}}" placeholder ="" style="margin-top: 15px;" >

              </div>
              
                 <div class="form-group col-md-6">
                  <input type="number" name="bidding_price" class="form-control" id="name" placeholder ="السعر الإبتدائي" required style="margin-top: 15px;">
                </div>
                
                <div class="form-group col-md-6">
                  <input type="number" name="added_value" class="form-control" id="name" required placeholder="قيمة المزايدة" style="margin-top: 15px;">
                </div>
                <div class="form-group col-md-6">
                  <input type="text" name="start_date" input type="text" class="form-control" onfocus="(this.type='datetime-local')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ البدء" required style="margin-top: 15px;">
                </div>
                <div class="form-group col-md-6">
                  <input type="text" input type="text" class="form-control" onfocus="(this.type='datetime-local')" onblur="if(!this.value)this.type='text'" name="exp_date" id="email" required placeholder="تاريخ الإنتهاء" required style="margin-top: 15px;">
                </div>
                
              </div>
              
              
              <div class="text-center"><button type="submit" style="margin-top: 15px;">حفظ</button></div>
              @if ($errors!=null)
              <div class="alert alert-danger">
                <ul>
                  <li>
                    {{ $errors }}
                  </li>
                </ul>
              </div><br />
              @endif
            </form>
          </div>
        </section>
      </div>
      
    </div>
    @endsection