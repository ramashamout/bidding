@extends('master1')
@section('contents')
      <!-- ======= Why Us Section ======= -->
      <section id="why-us" class="why-us section-bg">
         <div class="container-fluid" data-aos="fade-up">
            <div class="row justify-content-center">
               <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                  <section id="contact" class="contact">
                     <div  class="container"  >
                          <form action="{{route('piece.update',$piece->id)}}" method="post" role="form" enctype="multipart/form-data"  class="display">
                                  @method('PATCH')
                                  @csrf
            <div class="row">

              <div class="form-group">
                <label for="name">الوصف </label>
                <input type="text" class="form-control" name="discription" id="subject" value="{{$piece->discription}}"  placeholder="">
              </div>
              <div class="form-group col-md-6">
                <label for="name" style="margin-top: 8px;">التصنيف</label>
                  <select name="sub_section_id" class="form-control" value="{{$piece->sub_section->name}}">
                      @foreach($piece->sub_section->main_sections->sub_sections as $sub_s)
                          <option value="{{$sub_s->id}}">{{$sub_s->name}}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group col-md-6">
                <label for="name"style="margin-top: 8px;"> السعر الإبتدائي</label>
                <input type="number" name="bidding_price" class="form-control" id="name"  value="{{$piece->bidding_price}}">
              </div>
             <!--  <div class="form-group col-md-6">
                <label for="name"style="margin-top: 8px;">السعر المتوقع </label>
                <input type="number" class="form-control" name="expecting_price" id="email" value="{{$piece->expecting_price}}" >
              </div> -->
              <div class="form-group col-md-6">
                <label for="name"style="margin-top: 8px;"> قيمة المزايدة</label>
                <input type="number" name="added_value" class="form-control" id="name" value="{{$piece->added_value}}" >
              </div>

               
              <div class="form-group col-md-6">
                <label for="name"style="margin-top: 8px;">تاريخ البدء </label>
                <input type="datetime-local" name="start_date" class="form-control"  value="{{$piece->start_date}}" >
              </div>
              <div class="form-group col-md-6">
                <label for="name"style="margin-top: 8px;">تاريخ الإنتهاء </label>
                <input type="datetime-local" class="form-control" name="exp_date"  value="{{$piece->exp_date}}"  >
              </div>
               <div class="form-group  " style="  padding: 30px;">
                    <div class=" fileUpload btn btn-light">
                        <label class="upload">
                            <input  name="images[]"  type="file" class="form-control mx-sm-3" multiple>تحميل الصور </label>
                    </div>
                </div>

            </div>


            <div class="text-center">
              <button type="submit">حفظ</button>

            </div>
            <br>
             @if ($errors!=null)
    <div class="alert alert-danger">
        <ul>
            <li>
                {{ $errors }}
            </li>
        </ul>
    </div><br />
@endif

          </form>
                     </div>
                  </section>
               </div>

            </div>
         </div>
      </section>


@endsection
