@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        <!-- <div class="d-flex justify-content-center justify-content-lg-start">
          <a href="#about" class="btn-get-started scrollto">Get Started</a>
          <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
        </div> -->
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="/assets/img/usercontrol.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>إدارة الصلاحيات</h2>
      
    </div>
   <div class="row mt justify-content-center">
          <div class="col-lg-8 " data-aos="fade-up" data-aos-delay="200">
            <div class="content-panel">
              <table class="table table-striped table-advance table-hover">
               
                <hr>
                <thead>
                  <tr>
                    <th class="text-center"><i class="bi bi-person-fill" ></i>الصلاحيات</th>
                    <th class="text-center"></i>العمليات</th>
                   
                    
                  </tr>
                </thead>
                <tbody>
                       @foreach ($roles as $key => $role)
                  <tr>
                    <td class="text-center">
                      <h6 style=" color: #37517e;">{{ $role->name }}</h6>
                    </td>
                    <td class="text-center">
                           <form action="{{ route('roles.destroy',$role->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-xs " class="button"
                                        method="post" href="{{ route('roles.destroy',$role->id) }}" style="margin-right:-5px; ">
                                        <i class="bi bi-trash"></i>
                                        </button>
                                      </form>
                                <button class=" btn-success btn-xs dashbtn">
                                  <i class=" bi bi-pencil"></i></button></a>
                    </td>
                   
                   
                  </tr>
                  @endforeach
                
                </tbody>
              </table>
            </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-md-12 -->
        </div>
  </div>
</section>
@endsection