@extends('master') @section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center" style="font-family: font-family: 'Droid Arabic Kufi', serif;">
   <div class="container">
      <div class="row">
         <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
            <h1> مزادات مغرية  من أجلك </h1>
            <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
         </div>
         <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
            <img src="assets/img/banner-2.png" class="img-fluid animated" alt="">
         </div>
      </div>
   </div>
 </section><!-- End Hero -->
   <!-- ======= Portfolio Section ======= -->
   <section id="pricing" class="pricing" style="font-family: 'Droid Arabic Kufi', serif;">
      <div class="container" data-aos="fade-up">
         <!-- start size filter -->
         <!-- end filter -->
         <div class="section-title">
            <h2>القطع </h2>
            <p> يحتوي هذا القسم على القطع التالية </p>
         </div>
        
         <div class="row tab-content" data-aos="fade-up" data-aos-delay="200">
           
            <!-- start next bidding -->
            
               <div class="row" >
                  @foreach($pieces as $futur_piece)
                  <div class="col-lg-3" data-aos="fade-up" data-aos-delay="100" >
                     <div class="box">
                        <div class="portfolio-img"> <!--start slide image -->
                        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                           <div class="carousel-indicators">
                              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                           </div>
                           <div class="carousel-inner">
                              <div class="carousel-item active portfolio-img">
                                 <img  src="{{$futur_piece->images[0]->piece_img}}" class="card-img-top  " alt="...">
                              </div>
                              <div class="carousel-item portfolio-img">
                                 <img  src="{{$futur_piece->images[1]->piece_img}}" class="card-img-top  " alt="...">
                              </div>
                              <div class="carousel-item portfolio-img">
                                 <img  src="{{$futur_piece->images[2]->piece_img}}" class="card-img-top  " alt="...">
                              </div>
                           </div>
                           <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="visually-hidden">Previous</span>
                           </button>
                           <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="visually-hidden">Next</span>
                           </button>
                        </div>
                        <!--end slide image --></div>
                        <ul class="list-group list-group-flush task-list">
                           <li >
                              <div class="col-lg-12">
                                 <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                    <div class="pic">   <img src="/assets/img/desc2.jpg" class="rounded-circle" alt=""  style="width: 50px; margin: 10px;">
                                    </div>
                                    <div class="member-info" >
                                       <h4 >الوصف</h4>
                                       <!--  <span>فستان نهدي</span> -->
                                       <p>{{$futur_piece->discription}}</p>
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li >
                              <div class="col-lg-12">
                                 <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                    <div class="pic">   <img src="/assets/img/start.jpg" class="rounded-circle" alt=""  style="width: 50px; margin: 10px;">
                                    </div>
                                    <div class="member-info">
                                       <h4  > تاريخ البدء</h4>
                                       <!--  <span>فستان نهدي</span> -->
                                       <h4 style="color: #444444;font-size: 20px;">{{$futur_piece->start_date}}</h4 >
                                    </div>
                                 </div>
                              </div>
                           </li>
                           <li >
                              <div class="col-lg-12">
                                 <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                                    <div class="pic">   <img src="/assets/img/end.jpg" class="rounded-circle" alt=""  style="width: 50px; margin: 10px;">
                                    </div>
                                    <div class="member-info">
                                       <h4  >تاريخ الإنتهاء</h4>
                                       <!--  <span>فستان نهدي</span> -->
                                       <h4 style="color: #444444;font-size: 20px;">{{$futur_piece->exp_date}}</h4 >
                                    </div>
                                 </div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
                  @endforeach
               </div>
            
         </div>
         <!-- end next bidding -->
      </div>
   </section>
   <!-- End Portfolio Section -->
   @endsection