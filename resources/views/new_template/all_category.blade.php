@extends('master')
@section('contents')
<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
        <h1> مزادات مغرية  من أجلك </h1>
        <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
        
      </div>
      <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
        <img src="assets/img/dashboard.png" class="img-fluid animated" alt="">
      </div>
    </div>
  </div>
</section>
<!-- End Hero -->
<section id="contact" class="contact">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2>إدارة التصنيفات</h2>
    </div>
    <ul class="nav nav-pills mb-3 d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100"role="tablist">
      <li class="nav-item" role="presentation">
        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">إضافة تصنيف</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">جميع التصنيفات</button>
      </li>
    </ul>
    <div class="row tab-content" data-aos="fade-up" data-aos-delay="200">
      <!-- add action -->
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div class="row justify-content-center">
          <div class="col-lg-9 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="{{ route('sub_s.store') }}" method="post" role="form" enctype="multipart/form-data"  class="display">
              @csrf
              <div class="row">
                <div class="form-group ">
                  <select name="main_s_id" class="form-control">
                    @foreach($main_s as $main_s)
                    <option value="{{$main_s->id}}">{{$main_s->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group ">
                  <input type="text" name="name" class="form-control" id="name" placeholder ="اسم التصنيف" required style="margin-top: 15px;">
                </div>
                <div class="col-lg-6 col-md-6 col-6">
                  <div class="form-group  " style="  padding: 30px;">
                    <div class=" fileUpload btn btn-light">
                      <label class="upload">
                        <input name='image' type="file" class="form-control ">تحميل صورة </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="text-center"><button type="submit">حفظ</button></div>
              </form>
            </div>
          </div>
        </div>
        <!-- all action -->
        <div class="tab-pane fade " id="pills-contact" role="tabpanel" aria-labelledby="pills-home-tab">
          <div class="row">
            <section class="task-panel tasks-widget"style="border-top: 3px solid #47b2e4;">
              @foreach($section as $main_s)
              <div class="col-md-12 col-lg-12 col-12 title_action" >
                <h5> <img class="dashhammer" src="{{$main_s->main_s_img}}">{{$main_s->name}} </h5>
              </div>
              @foreach($main_s->sub_sections as $sub_s)
              <div class="col-md-12 col-lg-12 col-12">
                <div class="task-content">
                  <ul class="task-list">
                    <!--comming bid -->
                    <li>
                      <div class="task-title">
                        <span class="task-title-sp">
                          <i class=" bi bi-circle-fill" style="font-size: 10px;"></i>
                         <a data-bs-toggle="collapse" href="#collapseExample{{$sub_s->id}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$sub_s->id}}">
                            {{$sub_s->name}}
                            <button class=" btn-success btn-xs dashbtn">
                          <i class=" bi bi-pencil"></i></button></a>
                          
                          <div class="collapse" id="collapseExample{{$sub_s->id}}">

                            <div class="custom-check goleft mt">
                              <table id="todo" class="table table-hover custom-check">
                                <tbody>
                                  <tr>
                                    <td>
                                      <form method="post" action="{{route('sub_s.update',$sub_s->id)}}" enctype="multipart/form-data">
                                        @method('PATCH')
                                        @csrf
                                        <div class="row">
                                          <div class="col-lg-6 col-md-6 col-6">
                                            <div class="form-group  " style="  padding: 30px;">
                                              <input type="text" name="name" class="form-control" id="name" placeholder ="اسم القسم" value="{{$sub_s->name}}">
                                            </div>
                                          </div>
                                          <div class="col-lg-6 col-md-6 col-6">
                                            <div class="form-group  " style="  padding: 30px;">
                                              <div class=" fileUpload btn btn-light">
                                                <label class="upload">
                                                  <input name='image' type="file" class="form-control "value="{{$sub_s->main_s_id['main_s_img']}}">تحميل صورة </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="text-center">
                                            <button  class="close " type="submit">حفظ</button>
                                            <form action="{{ route('sub_s.destroy',$sub_s->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                           <button class=" close"   style="background: red;">حذف</button>
                          </form>
                                          </div>
                                        </form>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </span>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
                @endforeach
                @endforeach
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
    @endsection