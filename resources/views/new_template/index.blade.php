@extends('master') @section('contents')
<!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1> مزادات مغرية  من أجلك </h1>
          <h2>نحن نعمل باستمرار على جلب العروض الجديدة ، لذا استمر في زيارة موقعنا على الويب حتى لا تفوتك الفرصة التالية.</h2>
         
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/img/banner-2.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->
<section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <h3 >آلية العمل  </h3>
          <p>3 خطوات من أجل الربح</p>
        </header>

        <div class="row g-5">

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">
            <div class="box">
              <div >
 <div class="portfolio-img"><img src="assets/img/how1.png" class="img-fluid" alt=""></div>
              </div>
              <h4 class="title"><a href="/login">تسجيل الدخول</a></h4>
              <p class="description"> لا يتطلب بطاقة ائتمان  </p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4" data-aos="zoom-in" data-aos-delay="200">
             <div class="box">
              <div >
 <div class="portfolio-img"><img src="assets/img/how2.png" class="img-fluid" alt=""></div>
              </div>
              <h4 class="title"><a href="">المناقصة</a></h4>
              <p class="description">ادفع فقط إذا فزت </p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="box">
              <div >
 <div class="portfolio-img"><img src="assets/img/how3.png" class="img-fluid" alt=""></div>
              </div>
              <h4 class="title"><a href="">الفوز</a></h4>
              <p class="description"> المنافسة - عروض رائعة  </p>
            </div>
          </div>
          

        
        

      </div>


      </div>
    </section>
     
@endsection