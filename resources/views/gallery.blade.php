<!DOCTYPE html>
<html lang="en">
  <head>


    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">

    <link href="assets/css/style.css" rel="stylesheet">

  </head>

  <!-- End About Us Section -->
  <body>
    <div class="container">
      <div class="row">
        <div class="notify-row" >

          <div class="top-menu">
            <a id="toastbtn" >
              <i class="bi bi-bell" style="font-size: 25px;"></i>

              <span class="badge bg-warning num" style="position: absolute; ">{{auth()->user()->unreadNotifications->count()}}</span>
            </a>
          </div>
        </div>
        <div class="toast-container position-absolute top-0 start-0 p-3">
          <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
              <img src="..." class="rounded me-2" alt="...">
              <strong class="me-auto">Bootstrap</strong>
              <small class="text-muted">just now</small>
              <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            <div class="toast-body" >
              See? Just like this.
            </div>
          </div>

        </div>
        <!-- Toast -->
          <script srs="{{asset('js/app.js')}}"></script>
          <script src="js/app.js"></script>
          <script>
              window.Echo.private('result.{{auth()->id()}}').listen('PodcastResult',(e)=>{
                  console.log(e.result.content)
                  document.getElementsByClassName('num')[0].innerHTML= e.result.content;
              })
          </script>
      </div>
    </div>

    <!-- Vendor JS Files -->

    <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Template Main JS File -->

    <!-- <script type="text/javascript">
    window.onload = (event)=> {
    let myAlert = document.querySelector('.toast');
    let bsAlert = new  bootstrap.Toast(myAlert);
    bsAlert.show();

    }
    </script> -->
    <script>
    document.getElementById("toastbtn").onclick = function() {
    var toastElList = [].slice.call(document.querySelectorAll('.toast'))
    var toastList = toastElList.map(function(toastEl) {
    // Creates an array of toasts (it only initializes them)
    return new bootstrap.Toast(toastEl) // No need for options; use the default options
    });
    toastList.forEach(toast => toast.show()); // This show them

    console.log(toastList); // Testing to see if it works
    };
    </script>
  </body>
</html>
