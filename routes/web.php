<?php

use App\Models\Piece;
use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $all_sections=\App\Models\Main_section::all();
    $roles=\Spatie\Permission\Models\Role::all();

    return view('new_template.index',['all_sections'=>$all_sections,'roles'=>$roles]);

});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $now = Carbon::now();
    $date = Carbon::parse($now)->toDateTimeString();
    $pieces=Piece::all();
    $id=-1;
    foreach ($pieces as $piece){
        if($piece->exp_date >=$date && $piece->start_date <=$date){
            $id=$piece->id;
        }
    }
    $all_sections=\App\Models\Main_section::all();

    $pieces=\App\Models\Piece::all()->whereNotIn('id',$id);
        $roles=\Spatie\Permission\Models\Role::all();

    return view('new_template.index',['pieces'=>$pieces,'all_sections'=>$all_sections,'roles'=>$roles]);
});
    Route::resource('users', UserController::class);

Route::group(['middleware' => ['auth']], function() {


    Route::resource('roles', RoleController::class);



Route::get('/index', function () {
    $all_sections=\App\Models\Main_section::all();
        $roles=\Spatie\Permission\Models\Role::all();

    return view('new_template.index',['all_sections'=>$all_sections,'roles'=>$roles]);
});





Route::get('/get_piece/{piece}', 'App\Http\Controllers\HomeController@get_piece');



Route::get('/add/{id}', 'App\Http\Controllers\HomeController@add');
Route::post('/add_from_bride/{id}', 'App\Http\Controllers\PieceController@update_from_bride')->name('add_from_bride');

Route::get('/block/{id}', 'App\Http\Controllers\UserController@block')->name('block');
Route::get('/unblock/{id}', 'App\Http\Controllers\UserController@unblock')->name('unblock');
});









