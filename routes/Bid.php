<?php

use Illuminate\Support\Facades\Route;


Route::resource('bid','App\Http\Controllers\BidController');
Route::resource('main_s','App\Http\Controllers\MainSectionController');
Route::resource('sub_s','App\Http\Controllers\SubSectionController');
Route::resource('piece','App\Http\Controllers\PieceController');
Route::get('/sub_s_p/{id}','App\Http\Controllers\SubSectionController@show_s');
Route::get('/control_action/{id}','App\Http\Controllers\BidController@control_action' )->name('control_action');


Route::get('/all_section', 'App\Http\Controllers\MainSectionController@create');

Route::get('/all_category', 'App\Http\Controllers\SubSectionController@create');


Route::get('/control_users','App\Http\Controllers\MainSectionController@control_users')->name('control_users');
Route::get('/control_users_role','App\Http\Controllers\MainSectionController@control_users_role')->name('control_users_role');
Route::get('/control_role','App\Http\Controllers\MainSectionController@control_role');
Route::get('/actions', 'App\Http\Controllers\BidController@get_all_bid');



