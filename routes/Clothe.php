<?php

use Illuminate\Support\Facades\Route;


Route::resource('bid','App\Http\Controllers\BidController');
Route::resource('main_s','App\Http\Controllers\MainSectionController');
Route::resource('sub_s','App\Http\Controllers\SubSectionController');

Route::get('/u_notif','App\Http\Controllers\BidController@show_notif')->name('notif');
Route::get('/futur','App\Http\Controllers\BidController@get_futur_bid')->name('futurBid');
Route::get('/old','App\Http\Controllers\BidController@get_old_bid')->name('oldBid');
Route::get('/markAsRead/{id}','App\Http\Controllers\BidController@mark_as_read')->name('markAsRead');

