<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     // $user = User::create([
     //        'name' => 'Admin', 
     //        'email' => 'admin@admin.com',
     //        'password' => bcrypt('123456789'),
     //        'type'=>'admin'
     //    ]);
    $user=User::where('type','admin')->first();
        
         

        $permissions = Permission::pluck('id','id')->all();
        $role = Role::create(['name' => 'مدير']);
        $role->syncPermissions($permissions);
         

        $user->syncPermissions($permissions);
     
        $user->assignRole([$role->id]);
    }
}
