<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('bids', function (Blueprint $table) {
            $table->id();
            $table->integer('piece_id');
            $table->integer('bid_amount')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('bidder')->nullable();
            $table->timestamp('bid_datetime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
