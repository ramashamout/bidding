<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('pieces', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->integer('sub_section_id');
            $table->integer('winner_id')->nullable();
            $table->string('discription');
            $table->integer('bidding_price');
            $table->integer('expecting_price');
            $table->integer('added_value');
            $table->dateTime('start_date');
            $table->dateTime('exp_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pieces');
    }
}
