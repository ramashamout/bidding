<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Events\PodcastTime;
use App\Models\Bid;
use App\Models\Image;
use App\Models\Main_section;
use App\Models\Piece;
use App\Models\Sub_section;
use App\Models\User;
use App\Notifications\ResultNotification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use File;

class BidController extends Controller
{
       function __construct()
     {

         $this->middleware('permission:إدارة المزادات  ', ['only' => ['']]);

     }
    public function index(){
        $now = Carbon::now();
        $date = Carbon::parse($now)->toDateTimeString();
        $pieces=Piece::all();
        $all_sections=Main_section::all();
          $roles=\Spatie\Permission\Models\Role::all();

        if (Piece::count()==0){
            return view('new_template.endTime',['all_sections'=>$all_sections,'roles'=>$roles]);
        }
        foreach ($pieces as $piece){
            if($piece->exp_date >=$date && $piece->start_date <=$date){
                $bid= $piece->bid->last();
                if ($bid->bid_datetime !=null){
                    $bid_dt=explode(" ", $bid->bid_datetime);
                    $bid_date=$bid_dt[0];
                    $bid_time=$bid_dt[1];
                }


              //  $piece=Piece::all()->last();
                $da = explode(" ", $piece->exp_date);
                //$date=$da[0];
                $date = explode("-", $da[0]);
                $day = $date[2];
                $month = $date[1];
                $year = $date[0];

                $time = explode(":", $da[1]);
                $second=$time[2];
                $minute=$time[1];
                $hour=$time[0];
                if ($bid->bid_datetime !=null) {

                    return view('new_template.live_action', ['bid' => $bid, 'bid_date' => $bid_date, 'bid_time' => $bid_time, 'day' => $day, 'month' => $month, 'year' => $year, 'hour' => $hour, 'minute' => $minute, 'second' => $second, 'errors' => null,'all_sections'=>$all_sections,'roles'=>$roles]);
                }

                return view('new_template.live_action', ['bid' => $bid, 'day' => $day, 'month' => $month, 'year' => $year, 'hour' => $hour, 'minute' => $minute, 'second' => $second, 'errors' => null,'all_sections'=>$all_sections,'roles'=>$roles]);

            }
        }

            return view('new_template.endTime',['all_sections'=>$all_sections,'roles'=>$roles]);



    }
    public function create(){
        $now = Carbon::now();
        $date = Carbon::parse($now)->toDateString();
      //  $piece=Piece::all()->last();
        $main_s=Main_section::all();
        $all_sections=Main_section::all();
          $roles=\Spatie\Permission\Models\Role::all();

        return view('bid.addBid',['main_s'=>$main_s,'all_sections'=>$all_sections,'roles'=>$roles]);

    }
    public function store(Request $request){
        $roles=\Spatie\Permission\Models\Role::all();
        $ss=  Sub_section::find($request->get('sub_section_id'));
        $id=$ss->main_sections->id;
        $current_s=Main_section::find($id);
        $futur_piece = array();
        $old_piece = array();
        foreach ($current_s->sub_sections as $sub){
            foreach ($sub->pieces as $p){
                if($p->start_date > Carbon::now()){
                    array_push($futur_piece,$p);
                }
                elseif($p->exp_date < Carbon::now()){
                    array_push($old_piece,$p);
                }
            }
        }
        $all_piece=Piece::all();
        $piece=new Piece();
        $sub=Sub_section::find($request->get('sub_section_id'));
        $all_sections=Main_section::all();
        $piece->discription=$request->get('discription');
        $piece->sub_section_id=$request->get('sub_section_id');
        $piece->bidding_price=$request->get('bidding_price');
        $piece->expecting_price=0;//$request->get('expecting_price');
        $piece->added_value=$request->get('added_value');
        if($all_piece->count()!=0){
            foreach ($all_piece as $one){
                if($one->exp_date >=$request->get('start_date') &&$one->start_date <=$request->get('start_date')){
                    return view('new_template.control_action',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'all_sections'=>$all_sections,'roles'=>$roles,'current_s'=>$current_s,'errors' =>'يوجد مزاد اخر في هذا التاريخ']);

                }elseif ($one->exp_date >=$request->get('exp_date') &&$one->start_date <=$request->get('exp_date')){
                    return view('new_template.control_action',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'all_sections'=>$all_sections,'roles'=>$roles,'current_s'=>$current_s,'errors' => 'يوجد مزاد اخر في هذا التاريخ']);

                }elseif($request->get('exp_date') >=$one->start_date &&$request->get('start_date') <=$one->start_date){
                    return view('new_template.control_action',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'all_sections'=>$all_sections,'roles'=>$roles,'current_s'=>$current_s,'errors' => 'يوجد مزاد اخر في هذا التاريخ']);

                }
            }
        }
        if($request->get('start_date')< Carbon::now() || $request->get('exp_date')<$request->get('start_date')){
            return view('new_template.control_action',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'all_sections'=>$all_sections,'roles'=>$roles,'current_s'=>$current_s,'errors' => 'تاريخ غير صالح']);

        }
        $piece->start_date=$request->get('start_date');
        $piece->exp_date=$request->get('exp_date');

        $piece->save();
        if($files=$request->file('images')){
        foreach($files as $image){

            $image->move('bid_img\\', $image->getClientOriginalName().'.'.$image->getClientOriginalExtension());
            $img=new Image();
            $img->piece_id=$piece->id;
            $img->piece_img='\bid_img\\'.$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
            $img->save();
        }}

        $bid= new Bid();
        $bid->piece_id=$piece->id;
        $bid->bid_amount=$piece->bidding_price;
        $bid->save();

        $date = explode("-", $piece->exp_date);

        $day = $date[2];
        $month = $date[1];
        $year = $date[0];

        return back();
    }
    public function update(Request $request,$id){
        $roles=\Spatie\Permission\Models\Role::all();
        $all_sections=Main_section::all();
        if(Auth::user()==null){
            return redirect('login');
        }
        $now = Carbon::now();
        $date_now = Carbon::parse($now)->toDateTimeString();

        $pieces=Piece::all();
        foreach ($pieces as $p){
            if($p->exp_date >=$date_now && $p->start_date <=$date_now){
                $piece=$p;
            }
        }
        
        $da = explode(" ", $piece->exp_date);

        $date = explode("-", $da[0]);
        $day = $date[2];
        $month = $date[1];
        $year = $date[0];
        $time = explode(":", $da[1]);
        $second=$time[2];
        $minute=$time[1];
        $hour=$time[0];
        if($piece->exp_date >=$date_now) {
            $old_bid= Bid::all()->last();
            if ($old_bid->bid_datetime !=null) {
            $bid_dt=explode(" ", $old_bid->bid_datetime);
            $bid_date=$bid_dt[0];
            $bid_time=$bid_dt[1];
            }
            $bid= new Bid();

            $bid->piece_id=$piece->id;
            if($request->get('value')<=0){
                if ($old_bid->bid_datetime !=null) {
                return view('new_template.live_action',['bid'=>$old_bid,'bid_date'=>$bid_date,'bid_time'=>$bid_time,'day'=>$day,'month'=>$month,'year'=>$year,'hour'=>$hour,'minute'=>$minute,'second'=>$second,'errors'=>'invalid value','all_sections'=>$all_sections,'roles'=>$roles]);
                } return view('new_template.live_action',['bid'=>$old_bid,'day'=>$day,'month'=>$month,'year'=>$year,'hour'=>$hour,'minute'=>$minute,'second'=>$second,'errors'=>'invalid value','all_sections'=>$all_sections,'roles'=>$roles]);

            }

            $bid->bid_amount=$old_bid->bid_amount+$request->get('value');
            $bid->user_id=Auth::user()->id;
            $bid->bidder=Auth::user()->name;
            if(Auth::user()->id == $old_bid->user_id){
                if ($old_bid->bid_datetime !=null) {

                return view('new_template.live_action',['bid'=>$old_bid,'bid_date'=>$bid_date,'bid_time'=>$bid_time,'day'=>$day,'month'=>$month,'year'=>$year,'hour'=>$hour,'minute'=>$minute,'second'=>$second,'errors'=>'لا تستطيع المزايدة على نفسك','all_sections'=>$all_sections,'roles'=>$roles]);
               }
                return view('new_template.live_action',['bid'=>$old_bid,'day'=>$day,'month'=>$month,'year'=>$year,'hour'=>$hour,'minute'=>$minute,'second'=>$second,'errors'=>'لا تستطيع المزايدة على نفسك','all_sections'=>$all_sections,'roles'=>$roles]);
            }
            $currentTime = Carbon::now();
            $currentTime->toDateTime();
            $bid->bid_datetime=$currentTime;
            $bid_dt=explode(" ", $currentTime);
            $bid_date=$bid_dt[0];
            $bid_time=$bid_dt[1];
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $piece->exp_date);
            $from =  Carbon::now();
            $diff = $to->diffInSeconds($from);

            if($diff<=60){
                $newTime = date("Y-m-d H:i:s",strtotime("+15 minutes", strtotime($piece->exp_date)));
                $piece->exp_date=$newTime;
                broadcast(new PodcastTime($newTime))->toOthers();
                $piece->save();
                $da = explode(" ", $piece->exp_date);
                $date = explode("-", $da[0]);
                $day = $date[2];
                $month = $date[1];
                $year = $date[0];
                $time = explode(":", $da[1]);
                $second=$time[2];
                $minute=$time[1];
                $hour=$time[0];

                $bid->save();
                broadcast(new NewMessage($bid))->toOthers();
                return view('new_template.live_action',['bid'=>$bid,'bid_date'=>$bid_date,'bid_time'=>$bid_time,'day'=>$day,'month'=>$month,'year'=>$year,'hour'=>$hour,'minute'=>$minute,'second'=>$second,'errors'=>null,'all_sections'=>$all_sections,'roles'=>$roles]);

            }

            $bid->save();
            broadcast(new NewMessage($bid))->toOthers();


            return view('new_template.live_action',['bid'=>$bid,'bid_date'=>$bid_date,'bid_time'=>$bid_time,'day'=>$day,'month'=>$month,'year'=>$year,'hour'=>$hour,'minute'=>$minute,'second'=>$second,'errors'=>null,'all_sections'=>$all_sections,'roles'=>$roles]);
        }else{
            return view('new_template.live_action',['text'=>'time out','roles'=>$roles]);
        }
    }

    public function destroy($id)
    {
         $piece=Piece::find($id);
         foreach ($piece->images as $img){
             File::delete(public_path($img->piece_img));
         }
         $piece->delete();
         return redirect()->back();
    }
    public function show_notif(){
        $user=Auth::user();
        return view('bid.notification',['user'=>$user]);
    }
    public function mark_as_read($id) {
        $user=\App\Models\User::find(auth()->user()->id);
        $user->unreadNotifications->where('id', $id)->markAsRead();
        return redirect()->back();
    }

    public function get_futur_bid(){
       return $piece=Piece::all()->where('start_date', '>', Carbon::now());
    }
    public function get_old_bid(){
       return $piece=Piece::all()->where('exp_date', '<', Carbon::now());
    }
    public function get_all_bid(){
       $roles=\Spatie\Permission\Models\Role::all();
         $futur_piece=Piece::all()->where('start_date', '>', Carbon::now());
        $old_piece=Piece::all()->where('exp_date', '<', Carbon::now());
        $pieces=Piece::all();
        $now = Carbon::now();
        $date = Carbon::parse($now)->toDateTimeString();
        $current_piece=null;
        foreach ($pieces as $piece){
            if($piece->exp_date >=$date && $piece->start_date <=$date){
                $current_piece=$piece;
            }
        }
        $all_sections=Main_section::all();
        return view('new_template.actions',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'current_piece'=>$current_piece,'all_sections'=>$all_sections,'roles'=>$roles]);

    }
    public function control_action($id){
        $roles=\Spatie\Permission\Models\Role::all();
        $all_sections=Main_section::all();
        $current_s=Main_section::find($id);
        $futur_piece = array();
        $old_piece = array();
        foreach ($current_s->sub_sections as $sub){
            foreach ($sub->pieces as $p){
                if($p->start_date > Carbon::now()){
                    array_push($futur_piece,$p);
                }
                elseif($p->exp_date < Carbon::now()){
                    array_push($old_piece,$p);
                }
            }
        }

        return view('new_template.control_action',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'all_sections'=>$all_sections,'roles'=>$roles,'current_s'=>$current_s,'errors' => null]);
    }
}
