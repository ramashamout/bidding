<?php

namespace App\Http\Controllers;

use App\Models\Bid;
use App\Models\Image;
use App\Models\Main_section;
use App\Models\Piece;
use App\Models\Sub_section;
use Illuminate\Http\Request;
use Carbon\Carbon;
use File;
use Illuminate\Support\Facades\Http;

class PieceController extends Controller
{
    public function edit($id)
    {
        $piece = Piece::find($id);
        $all_sections=\App\Models\Main_section::all();
         $roles=\Spatie\Permission\Models\Role::all();

        return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>null ,'roles'=>$roles]);
    }
    public function update(Request $request,$id){
        $all_piece=Piece::all();
        $piece=Piece::find($id);
        $all_sections=Main_section::all();
         $roles=\Spatie\Permission\Models\Role::all();

        $piece->discription=$request->get('discription');
        $request->get('sub_section_id');
        $piece->bidding_price=$request->get('bidding_price');
//        $piece->expecting_price=0;//$request->get('expecting_price');
        $piece->added_value=$request->get('added_value');

        if($request->get('start_date')!=null && $request->get('exp_date')!=null){
            if($all_piece->count()!=0){
                foreach ($all_piece as $one){
                    if ($piece->id!=$one->id){
                        if($one->exp_date >=$request->get('start_date') &&$one->start_date <=$request->get('start_date')){
//                            return 'invalied date';
//                            return back()->with(['errors'=>'there is another bid in this date']);
                            return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'يوجد مزاد آخر في هذا التاريخ' ,'roles'=>$roles]);
                        }
                        elseif ($one->exp_date >=$request->get('exp_date') &&$one->start_date <=$request->get('exp_date')){
//                            return 'invalied date';
                            return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'يوجد مزاد آخر في هذا التاريخ' ,'roles'=>$roles]);

                        }elseif($request->get('exp_date') >=$one->start_date &&$request->get('start_date') <=$one->start_date){
                            return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'يوجد مزاد آخر في هذا التاريخ' ,'roles'=>$roles]);

                        }

                    }

                }
            }
            if($request->get('start_date')< Carbon::now() || $request->get('exp_date')<$request->get('start_date')){
//                return 'invalied date';
                return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'تاريخ غير صالح' ,'roles'=>$roles]);
            }

            $piece->start_date=$request->get('start_date');
            $piece->exp_date=$request->get('exp_date');
        }
        elseif ($request->get('start_date')!=null){
            if($all_piece->count()!=0){
                foreach ($all_piece as $one){
                    if ($piece->id!=$one->id){
                        if($one->exp_date >=$request->get('start_date') &&$one->start_date <=$request->get('start_date')){
                            return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'يوجد مزاد آخر في  هذا التاريخ' ,'roles'=>$roles]);
                        }
                    }

                }
            }
            if($request->get('start_date')< Carbon::now() || $piece->exp_date < $request->get('start_date')){
                return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'تاريخ غير صالح' ,'roles'=>$roles]);
            }
            $piece->start_date=$request->get('start_date');
        }
        elseif ($request->get('exp_date')!=null){
            if($all_piece->count()!=0){
                foreach ($all_piece as $one){
                    if ($piece->id!=$one->id){
                        if ($one->exp_date >=$request->get('exp_date') &&$one->start_date <=$request->get('exp_date')){
                            return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'يوجد مزاد آخر في هذا التاريخ ' ,'roles'=>$roles]);
                        }
                    }

                }
            }
            if($request->get('exp_date') < $piece->start_date){
                return view('new_template.edit_action',['piece'=>$piece,'all_sections'=>$all_sections,'errors'=>'تاريخ غير صالح' ,'roles'=>$roles]);
            }

            $piece->exp_date=$request->get('exp_date');
        }

        $piece->save();
        if($files=$request->file('images')){
            foreach ($piece->images as $img){
                File::delete(public_path($img->piece_img));
                $img->delete();
            }
            foreach($files as $image){
//            $image=$request->file('images');
                $image->move('bid_img\\', $image->getClientOriginalName().'.'.$image->getClientOriginalExtension());
                $img=new Image();
                $img->piece_id=$piece->id;
                $img->piece_img='\bid_img\\'.$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
                $img->save();
            }}

        $bid= $piece->bid[0];

        $bid->bid_amount=$piece->bidding_price;
        $bid->save();


        return redirect(route('control_action',$piece->sub_section->main_sections));


    }


    public function show($id){
        $futur_piece=Piece::all()->where('sub_section_id',$id)
                                ->where('start_date', '>', Carbon::now());
        $old_piece=Piece::all()->where('sub_section_id',$id)
            ->where('exp_date', '<', Carbon::now());
        $pieces=Piece::all();
        $now = Carbon::now();
        $date = Carbon::parse($now)->toDateTimeString();
        $current_piece=null;
         $roles=\Spatie\Permission\Models\Role::all();

        foreach ($pieces as $piece){
            if($piece->exp_date >=$date && $piece->start_date <=$date && $piece->sub_section_id==$id){
                $current_piece=$piece;
            }
        }
        $all_sections=Main_section::all();
        return view('new_template.actions',['futur_piece'=>$futur_piece,'old_piece'=>$old_piece,'current_piece'=>$current_piece,'all_sections'=>$all_sections ,'roles'=>$roles]);


    }
    public function update_from_bride(Request $request,$id){
        $all_piece=Piece::all();

 $roles=\Spatie\Permission\Models\Role::all();


        $piece=new Piece();
        $all_sections=Main_section::all();
        $piece->discription=$request->get('discription');
        $piece->sub_section_id=$request->get('sub_section_id');
        $piece->bidding_price=$request->get('bidding_price');
        $piece->expecting_price=10;//$request->get('expecting_price');
        $piece->added_value=$request->get('added_value');

        if($all_piece->count()!=0){
            foreach ($all_piece as $one){
                if($one->exp_date >=$request->get('start_date') &&$one->start_date <=$request->get('start_date')){
                    return view('new_template.add_from_bride',['des'=>$request->get('discription'),'all_sections'=>$all_sections,'piece_bride_id'=>$id,'errors'=>'يوجد مزاد آخر في هذا التاريخ ' ,'roles'=>$roles]);

                }elseif ($one->exp_date >=$request->get('exp_date') &&$one->start_date <=$request->get('exp_date')){
                    return view('new_template.add_from_bride',['des'=>$request->get('discription'),'all_sections'=>$all_sections,'piece_bride_id'=>$id,'errors'=>'يوجد مزاد آخر في هذا التاريخ ' ,'roles'=>$roles]);

                }
            }
        }
        if($request->get('start_date')< Carbon::now() || $request->get('exp_date')<$request->get('start_date')){
            return view('new_template.add_from_bride',['des'=>$request->get('discription'),'all_sections'=>$all_sections,'piece_bride_id'=>$id,'errors'=>'تاريخ غير صالح' ,'roles'=>$roles]);

        }
        $piece->start_date=$request->get('start_date');
        $piece->exp_date=$request->get('exp_date');

        $piece->save();
        $response = Http::get('http://localhost:8000/api/send_piece/'.$id);
        $img_name =$response['piece_img'];

        \Illuminate\Support\Facades\File::copy('C:\Users\User\Desktop\allproject\bride_house\public\images\clothe\\'.$img_name , 'bid_img\\'.$img_name);
        $img=new Image();
        $img->piece_id=$piece->id;
        $img->piece_img='\bid_img\\'.$img_name;
        $img->save();
        foreach ($response['image'] as $img){
            $img_name =$img['src'];
 \Illuminate\Support\Facades\File::copy('C:\Users\User\Desktop\allproject\bride_house\public\images\clothe\\'.$img_name , 'bid_img\\'.$img_name);
            $img=new Image();
            $img->piece_id=$piece->id;
            $img->piece_img='\bid_img\\'.$img_name;
            $img->save();
        }
        $bid= new Bid();
        $bid->piece_id=$piece->id;
        $bid->bid_amount=$piece->bidding_price;
        $bid->save();
$s=Sub_section::find($piece->sub_section_id);

        return redirect(route('control_action',$s->main_s_id));
    }

}
