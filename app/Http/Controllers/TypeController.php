<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{

    public function index()
    {
        $type = Type::all();
        return view('type.index', compact('type')); // -> resources/views/stocks/index.blade.php
    }

    public function create()
    {
        return view('type.create'); // -> resources/views/stocks/create.blade.php

    }

    public function store(Request $request)
    {
        // Validation for required fields (and using some regex to validate our numeric value)
        $request->validate([
            'name'=>'required'
        ]);
        // Getting values from the blade template form
        $type = new Type([
            'name' => $request->get('name')
        ]);
        $type->save();
        return redirect('/type')->with('success', 'type saved.');   // -> resources/views/stocks/index.blade.php
    }

    public function show($id)
    {
        $selected_Item = Type::find($id);
    }

    public function edit($id)
    {
        $type = Type::find($id);
        return view('type.edit', compact('type'));  // -> resources/views/stocks/edit.blade.php
    }

    public function update(Request $request, $id)
    {
//         Validation for required fields (and using some regex to validate our numeric value)
        $request->validate([
            'name'=>'required'
        ]);
        $type = Type::find($id);
        // Getting values from the blade template form
        $type->name =  $request->get('name');
        $type->save();

        return redirect('/type')->with('success', 'type updated.'); // -> resources/views/stocks/index.blade.php
    }

    public function destroy($id)
    {
        $type = Type::find($id);
        $type->delete();

        return redirect('/type')->with('success', 'type removed.');  // -> resources/views/stocks/index.blade.php
    }

}
