<?php

namespace App\Http\Controllers;

use App\Models\Main_section;
use Illuminate\Http\Request;
use File;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use DB;
class MainSectionController extends Controller
{

      function __construct()
    {
         $this->middleware('permission:إدارة الأقسام ', ['only' => ['create','store']]);
     
    }
      public function index(){
        $all_sections=\App\Models\Main_section::all();
        $main_s=\App\Models\Main_section::all();
          $roles=\Spatie\Permission\Models\Role::all();

        return view('new_template.section',['all_sections'=>$all_sections,'roles'=>$roles,'main_s'=>$main_s]);
    }
    public function create(){
        $section=Main_section::all();
        $all_sections=Main_section::all();
         $roles=\Spatie\Permission\Models\Role::all();
        return view('new_template.all_section',['section'=>$section,'roles'=>$roles,'all_sections'=>$all_sections]);
    }
    public function store(Request $request){
        $main_s=new Main_section();
        $main_s->name=$request->get('name');
        $image=$request->file('image');
        $image->move('main_img\\', $image->getClientOriginalName().'.'.$image->getClientOriginalExtension());
        $main_s->main_s_img='\main_img\\'.$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
        $main_s->save();
        $section=Main_section::all();
        return redirect('/all_section');
    }
    public function update(Request $request,$id){
        $main_s=Main_section::find($id);
        if($request->get('name')!=null){
            $main_s->name=$request->get('name');
        }if($image=$request->file('image')!=null){
            $image=$request->file('image');
            $image->move('main_img\\', $image->getClientOriginalName().'.'.$image->getClientOriginalExtension());
            $main_s->main_s_img='\main_img\\'.$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
        }
        $main_s->save();
        return redirect('/all_section');
    }

    public function destroy($id)
    {
        $main_s=Main_section::find($id);
        File::delete(public_path($main_s->main_img));
        $main_s->delete();

        return redirect('/all_section');
    }
    public function control_users()
    {
        $all_sections=\App\Models\Main_section::all();
        $user=User::all();
         $roles = Role::pluck('name','name')->all();
         $data = User::orderBy('id','DESC')->paginate(5);
         // $userRole = $user->roles->pluck('name','name')->all();
          if(Auth::user()){
        return view('new_template.control_users',['all_sections'=>$all_sections,'user'=>$user,'roles'=>$roles,'data'=>$data]);}
          else return redirect('/login');
    }
     public function control_users_role()
    {
        $all_sections=\App\Models\Main_section::all();
        $user=User::all();
         $roles = Role::pluck('name','name')->all();
         $data = User::orderBy('id','DESC')->paginate(5);
         // $userRole = $user->roles->pluck('name','name')->all();
          if(Auth::user()){
        return view('new_template.control_users_role',['all_sections'=>$all_sections,'user'=>$user,'roles'=>$roles,'data'=>$data]);}
          else return redirect('/login');
    }
    public function edit_users($id)
    {
        $all_sections=\App\Models\Main_section::all();
       $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
       
    
        
          if(Auth::user()){
  return view('new_template.edit_user',['all_sections'=>$all_sections,'user'=>$user,'roles'=>$roles,'userRole'=>$userRole]);}
          else return redirect('/login');
    }
  
 


}
