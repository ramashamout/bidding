<?php   
namespace App\Http\Controllers;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Models\Main_section;
use Spatie\Permission\Models\Permission;
use DB;
use Hash;
use Illuminate\Support\Arr;
    
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
      function __construct()
    {
         $this->middleware('permission:إدارة المستخدمين ', ['only' => ['create','edit','update']]);
     
    }
    public function display()
    {
        $user=User::all();
        $roles=Role::all();
        return view('new_template.control_users',['user'=>$user,'roles'=>$roles]);
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
     public function block($id)
    {
        $user = User::find($id);
        $user->status=0;
        $user->save();
        return back();
    }
       public function unblock($id)
    {
        $user = User::find($id);
        $user->status=1;
        $user->save();
        return back();
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
    {
         
         $data = User::orderBy('id','DESC')->paginate(5);
        $all_sections=Main_section::all();
         $roles=\Spatie\Permission\Models\Role::all();

        return view('users.add_user',['all_sections'=>$all_sections,'data'=>$data,'roles'=>$roles]);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
                   ]);
      
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $request->type='user';
        $input['type'] =$request->type;
         $request->image_path='images/33.png';
       $input['image_path'] =$request->image_path;

        $role = Role::find(1);
         $permissions = Permission::pluck('id','id')->all();
        

        $user = User::create($input);
        $user->assignRole([$role->id]);
        Auth::loginUsingId($user->id);

      
        return redirect('/dashboard');    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user = User::find($id);
        $roles = Role::all();
        $userRoles = DB::Connection('mysql2')->table("model_has_roles")->where("model_has_roles.model_id",$id)
            ->pluck('model_has_roles.role_id','model_has_roles.role_id')
            ->all();
        $all_sections=\App\Models\Main_section::all();
    
        return view('new_template.edit_user',compact('user','roles','userRoles','all_sections'));
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    

        $input = $request->all();
       
    
        $user = User::find($id);
        $user->update($input);
        DB::Connection('mysql2')->table('model_has_roles')->where('model_id',$id)->delete();
    
        $user->assignRole($request->input('roles'));
    
        return redirect()->route('control_users');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}