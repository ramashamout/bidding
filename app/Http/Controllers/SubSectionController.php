<?php

namespace App\Http\Controllers;

use App\Models\Main_section;
use App\Models\Sub_section;
use App\Models\Piece;
use Illuminate\Http\Request;
use File;
class SubSectionController extends Controller
{
       function __construct()
    {
         $this->middleware('permission:إدارة التصنيفات ', ['only' => ['create','store']]);

    }
    public function index(){
        $all_sections=\App\Models\Main_section::all();
         $roles=\Spatie\Permission\Models\Role::all();
        
        $sub_s=\App\Models\Main_section::all();
        return view('new_template.section',['all_sections'=>$all_sections,'sub_s'=>$sub_s,'roles'=>$roles]);
    }
    public function create(){
        $section=Main_section::all();
        $main_s=Main_section::all();
        $all_sections=Main_section::all();
         $roles=\Spatie\Permission\Models\Role::all();

        return view('new_template.all_category',['section'=>$section,'main_s'=>$main_s,'all_sections'=>$all_sections,'roles'=>$roles]);
    }
    public function store(Request $request){

        $sub_s=new Sub_section();
        $sub_s->name=$request->get('name');
        $sub_s->main_s_id=$request->get('main_s_id');
        $image=$request->file('image');
        $image->move('sub_img\\', $image->getClientOriginalName().'.'.$image->getClientOriginalExtension());
        $sub_s->sub_s_img='\sub_img\\'.$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
        $sub_s->save();
        return redirect('/all_category');

    }
    public function update(Request $request,$id){
        $sub_s=Sub_section::find($id);
        if($request->get('name')!=null){
            $sub_s->name=$request->get('name');
        }if($request->get('main_s_id')!=null){
            $sub_s->main_s_id=$request->get('main_s_id');
        }if( $image=$request->file('image')!=null){
            $image=$request->file('image');
            $image->move('main_img\\', $image->getClientOriginalName().'.'.$image->getClientOriginalExtension());
            $sub_s->sub_s_img='\main_img\\'.$image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
        }
        $sub_s->save();
        return redirect('/all_category');
    }
    public function show($id)
    {
         $roles=\Spatie\Permission\Models\Role::all();

        $main_s=Main_section::find($id);
//        return ['sub_s'=>$main_s->sub_sections];
        $all_sections=\App\Models\Main_section::all();
       // $sub_s=\App\Models\Main_section::all();
        return view('new_template.sub_section',['all_sections'=>$all_sections,'main_s'=>$main_s,'roles'=>$roles]);
    }
    public function show_s($id)
    {
         $roles=\Spatie\Permission\Models\Role::all();

        $sub=Sub_section::find($id);
        $pieces=$sub->pieces;
         $all_sections=\App\Models\Main_section::all();
        return view('new_template.sub_s_p',['pieces'=>$pieces,'all_sections'=>$all_sections,'roles'=>$roles]);
    }
//
//    public function edit($id)
//    {
//        $sub_s=Sub_section::find($id);
//        return view(['sub_s'=>$sub_s]);
//    }
    public function destroy($id)
    {
        $sub_s=Sub_section::find($id);
        File::delete(public_path($sub_s->sub_s_img));
        $sub_s->delete();
        return redirect('/all_category');
    }

}
