<?php

namespace App\Http\Controllers;

use App\Models\Bid;
use App\Models\Image;
use App\Models\Piece;
use Illuminate\Http\Request;
use App\Models\Main_section;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    //
    // public function index()
    // {
    // 	$all_sections=Main_section::all();
    // 	return view('new_template.index',['all_sections'=>$all_sections]);
    // }
   
    public function get_piece($piece){
return 'done';
        $manage = json_decode($piece, true);

        $dis='هذه القطعة من  '.$manage['clothes_type'].' لون '.$manage['color'].' من تصميم المصمم  '.$manage['designer_name'].' و قياسها '.$manage['size'];

        $piece=new Piece();
        $piece->discription=$dis;
        $piece->sub_section_id=1;
        $piece->bidding_price=0;
        $piece->expecting_price=0;
        $piece->added_value=0;
        $piece->start_date='2022-07-10 19:36:22';
        $piece->exp_date='0-07-10 19:36:22';
        $piece->save();
        $imgarray =[];
        foreach ($manage['image'] as $img){
//            $date = explode("\\",$img['src']);
            $img_name =$img['src'];

            File::copy('C:\Users\User\Desktop\final\ja\bride_house\public\images\clothe\\'.$img_name , 'bid_img\\'.$img_name);
            $img=new Image();
            $img->piece_id=$piece->id;
            $img->piece_img='\bid_img\\'.$img_name;
            $img->save();
        }

  $roles=\Spatie\Permission\Models\Role::all();


return view('new_template.add_from_bride',['piece'=>$piece,'roles'=>$roles]);
    }

    public function add($id)
{

    $all_sections=Main_section::all();

  $roles=\Spatie\Permission\Models\Role::all();
    $response = Http::get('http://localhost:8000/api/send_description/'.$id);
    $des='هذه القطعة من  '.$response['clothes_type'].' لون '.$response['color'].' من تصميم المصمم  '.$response['designer_name'].' و قياسها '.$response['size'];

    return view('new_template.add_from_bride',['des'=>$des,'all_sections'=>$all_sections,'piece_bride_id'=>$id,'errors'=>null,'roles'=>$roles]);

}

}
