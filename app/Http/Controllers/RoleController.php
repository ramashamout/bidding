<?php
    
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Auth;
use DB;
    
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    function __construct()
    {
        
         $this->middleware('permission:إدارة الصلاحيات', ['only' => ['destroy']]);
       
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id','DESC')->paginate(5);
        return view('roles.index',compact('roles'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get();
        $user=Auth::User();
         $all_sections=\App\Models\Main_section::all();
         $roles=Role::all();
        
         if(Auth::user()){ 
       return view('roles.add_roles',['user'=>$user,'all_sections'=>$all_sections,'permission'=>$permission,'roles'=>$roles]);}
       else return redirect('/login');
    }
 
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
    
        $role = Role::on('mysql2')->create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        // $user=Auth::User();
        // $user->syncPermissions($request->input('permission'));
    
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();
         $user=Auth::User();
        $all_sections=\App\Models\Main_section::all();
 $roles=\Spatie\Permission\Models\Role::all();
        return view('roles.show_roles',['role'=>$role,'roles'=>$roles,'rolePermissions'=>$rolePermissions,'user'=>$user,'all_sections'=>$all_sections]);
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
         $user=Auth::User();
         $all_sections=\App\Models\Main_section::all();
        $rolePermissions = DB::Connection('mysql2')->table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
     $roles=\Spatie\Permission\Models\Role::all();

        return view('roles.edit_role',['role'=>$role,'roles'=>$roles,'permission'=>$permission,'user'=>$user,'rolePermissions'=>$rolePermissions,'all_sections'=>$all_sections]);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);
    
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
    
        $role->syncPermissions($request->input('permission'));
    
        return redirect('/roles/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::Connection('mysql2')->table("roles")->where('id',$id)->delete();
        return back();
    }
}