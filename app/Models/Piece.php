<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    use HasFactory;
 protected $connection="mysql2";
    public function bid()
    {
        return $this->hasMany(Bid:: Class,'piece_id');
    }
    public function images()
    {
        return $this->hasMany(Image:: Class,'piece_id');
    }
    public function winner()
    {
//        DB::connection('mysql')->table;
        return $this->belongsTo(User::Class,'winner_id');
    }
    public function sub_section()
    {
        return $this->belongsTo(Sub_section:: Class,'sub_section_id');
    }

     public function main_section()
    {
        return $this->belongsTo(Main_section:: Class,'sub_section_id');
    }

    protected  $table = 'pieces';
    // protected $fillable = [
    //     'name',
    //     'piece_img',
    //     'selling_price',
    //     'rent_price',
    //     'type'
    // ];
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($piece) { // before delete() method call this
            $piece->bid()->delete();
            $piece->images()->delete();
        });
    }
}
