<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sub_section extends Model
{
    use HasFactory;
     protected $connection="mysql2";
    public function main_sections()
    {
        return $this->belongsTo(Main_section:: Class,'main_s_id');
    }
    public function pieces()
    {
        return $this->hasMany(Piece:: Class,'sub_section_id');
    }
    protected  $table = 'sub_sections';
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($s_section) { // before delete() method call this
            $s_section->pieces()->delete();
        });
    }
}
