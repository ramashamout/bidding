<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Main_section extends Model
{
    use HasFactory;
     protected $connection="mysql2";
    public function sub_sections()
    {
        return $this->hasMany(Sub_section:: Class,'main_s_id');
    }
       public function piece()
    {
        return $this->hasMany(Piece:: Class,'sub_section_id');
    }
    protected  $table = 'main_sections';
    protected static function boot()
    {
        parent::boot();
        static::deleting(function($m_section) { // before delete() method call this
            $m_section->sub_sections()->delete();
        });
    }
}
