<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
     use Notifiable;
    use TwoFactorAuthenticatable;
use HasRoles;
    protected $connection="mysql";
    public function bid()
    {
        return $this->hasMany(Bid:: Class,'user_id');
    }
    public function piece()
    {
        return $this->hasMany(Piece:: Class,'winner_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
       protected $fillable = [
        'name', 'email', 'password', 'mobile_number',
        'username','profile_photo_path','remember_token','type','device_token','status'


    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
         'mobile_number',
        'username',

           ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
      protected $appends = [
        'profile_photo_path',
    ];
    public function getImagePathAttribute()
    {
        if ($this->profile_photo_path) {
           return Storage::url('upload/'.$this->profile_photo_path);
        }
        return asset('images/default.png');
    }

       public function appointments()
    {
        return $this->hasMany(Appointment::Class);
    }

}
