<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    use HasFactory;
     protected $connection="mysql2";
    public function piece()
    {
        return $this->belongsTo(Piece::Class,'piece_id');
    }
    public function user()
    {
        return $this->belongsTo(User::Class,'user_id');
    }
}
