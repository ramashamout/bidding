<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\VarDumper\Cloner\Data;

class PodcastTime implements ShouldBroadcast
{
    public $day,$month,$year,$second,$minute,$hour;
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($new_date)
    {
        $da = explode(" ", $new_date);
        $date = explode("-", $da[0]);
        $this->day = $date[2];
        $this->month = $date[1];
        $this->year = $date[0];
        $time = explode(":", $da[1]);
        $this->second=$time[2];
        $this->minute=$time[1];
      //  $int = (int)$time[0]+3;
        $this->hour=$time[0];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('time');
    }
}
