<?php

namespace App\Console;

use App\Models\Bid;
use App\Models\Piece;
use App\Models\User;
use App\Notifications\ResultNotification;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Http;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            $users=User::all()->where('type' ,'!=','admin')
            ->where('type' ,'!=','employ');
            $bool=false;
//            $piece_id=Piece::all()->last()->id;
            $last_b=Bid::all()->last();
            $piece_id=$last_b->piece->id;
            foreach ($users as $user){
                foreach ($user->notifications as $notif){
                    if($notif->data['piece_id']==$piece_id){
                        $bool=true;
                    }
                }
            }
            //if there is no notification yet
            if($bool==false){
                $last_b=Bid::all()->last();
                $piece=$last_b->piece;
//                $piece=Piece::all()->last();
                $now = Carbon::now();
                $date_now = Carbon::parse($now)->toDateTimeString();
                if($piece->exp_date < $date_now) {
                $result=['piece_id'=>$piece->id,'content'=>'تهانينا لقد فزت بالمزاد يمكنك استلام قطعتك في اي وقت','image'=>$piece->piece_img];


                if($piece->bid->count() >1) {
                    $bids=Bid::all()->last()->user_id;
                    $piece->winner_id=$bids;
                    $piece->save();
                    User::find($bids)->notify(new ResultNotification($result));
                    $c_user=User::find($bids);
                    $r=['content'=>$c_user->unreadNotifications->count(),'noti'=>$c_user->unreadNotifications->last()];
                    event(new \App\Events\PodcastResult($bids,$r));

                    if($piece->expecting_price==10){
                        $response = Http::get('http://localhost:8000/api/send_info/'.$bids.'/'.$last_b->bid_amount);
                    }
                }elseif ($piece->expecting_price==10){
                    if($piece->bid->count()==1){
                        $response = Http::get('http://localhost:8000/api/no_winner');
                    }
                }

                return "done";
            }
            }
            return "no";
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
